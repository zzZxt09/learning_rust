# Basic rust commands

## Build and Run

1. ```cargo build```:  build current rust directory.
2. ```cargo run```: run the executable
3. ```cargo build --release``` : build the release binary
4. ```cargo update```: get new dependency versions but not build it yet. 
5. ```cargo new $PROJECT_NAME``` : create new rust project.

## Documentation

1. ```rustup doc``` : open documents for current rust toolchain.
2. ```rustup doc --book``` : open the "The Rust Programming Language" book.
3. ```rustup doc --reference```: open rust's reference book(not for learning the language.)
4. ```rustup doc --cargo``` : open the cargo book.
5. ```rustup doc --core```: open the rust core documents.
6. ```rustup doc --rustdoc``` : 
7. ```rustup doc --std```: open documents for rust's std library
8. ```rustup doc --test``` : open documentation for rust's test framework
9. ```cargo doc --open``` : generate and open current rust project's documents as well as documents for the dependents.

# Toolchains

* ```rustup show```: show installed toolchain.

* Build with specific toolchain(arm cross-compile):

  ```
  cargo build --target armv7-unknown-linux-gnueabihf
  ```

  

## First Rust Code

```dust
use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {

    let secret_number = rand::thread_rng().gen_range(0,100);

    println!("Guess the number!");
    println!("Please input a number.");

    loop {
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("Failed to read number");
        println!("You guessed {}.", guess);
        let guess :u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Please input a valid unsigned number.");
                continue;
            }
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small."),
            Ordering::Greater => println!("Too big."),
            Ordering::Equal => {
                println!("You got it.");
                break;
            }
        }
    }

}

```