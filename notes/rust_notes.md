#  Variables :smile:

* Variables are immutable my default.

  ```rust
  let x = 5; // Immutable
  let mut x = 5; // Mutable
  ```

* Constants are always immutable(of course). Must be declared with type explicitly:

  ```rust
  const MAX_POINTS :u32 = 100_000; // underscore can be inserted in numbers to improve readability
  ```

  Constants can be declared at any scope, including global.

* Shadowing. Following code is OK:

  ```rust
      let x = 1;
      let x = x*2;
      let mut x = x*2;
      x = x+1;
  ```

  > Rustaceans say that the first variable is *shadowed* by the second, which means that the second variable’s value is what appears when the variable is used. We can shadow a variable by using the same variable’s name and repeating the use of the `let` keyword

  Shadowing also allows changing the type and mutability.



# DataType :crying_cat_face:

Rust is a **statically typed** language which means compiler must know every variable's type during compilation. In case many types are possible, type annotations must be added:

```rust
let x: u32 = "11".parse().expect("Not a valid number");
```

Ad this is equivalent with the "turbofish" syntax to explicitly tell the compiler which type to parse to:

```rust
let x = "11".parse::<u43>().expect"Not a valid number");
```

There are generally two data type subtypes: ***scalar and compound***.

## Scalar

Integers - floating point - boolean - characters

### Integer

The table:

| size | signed | unsigned |
| ---- | ------ | -------- |
| 8    | i8     | u8       |
| 16   | i16    | u16      |
| 32   | i32    | u32      |
| 64   | i64    | u64      |
| 128  | i128   | u128     |
| arch | isize  | usize    |

note: "isize" and "usize" depends on CPU architecture. isize is i64 on 64-bit system and i32 if 32-bit system.

Integer literals:

| Number Literals | Example    |
| --------------- | ---------- |
| Decimal         | 98_222     |
| Hex             | 0xff       |
| Octal           | 0o77       |
| Binary          | 0b111_0000 |
| Byte(u8 only)   | b'A'       |



Regarding overflow:

> > ##### [Integer Overflow](file:///home/zxt/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/doc/rust/html/book/ch03-02-data-types.html#integer-overflow)
> >
> > Let’s say you have a variable of type `u8` that can hold values between 0 and 255. If you try to change the variable to a value outside of that range, such as 256, *integer overflow* will occur. Rust has some interesting rules involving this behavior. When you’re compiling in debug mode, Rust includes checks for integer overflow that cause your program to *panic* at runtime if this behavior occurs. Rust uses the term panicking when a program exits with an error; we’ll discuss panics in more depth in the [“Unrecoverable Errors with `panic!`”](file:///home/zxt/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/doc/rust/html/book/ch09-01-unrecoverable-errors-with-panic.html) section in Chapter 9.
> >
> > When you’re compiling in release mode with the `--release` flag, Rust does *not* include checks for integer overflow that cause panics. Instead, if overflow occurs, Rust performs *two’s complement wrapping*. In short, values greater than the maximum value the type can hold “wrap around” to the minimum of the values the type can hold. In the case of a `u8`, 256 becomes 0, 257 becomes 1, and so on. The program won’t panic, but the variable will have a value that probably isn’t what you were expecting it to have. Relying on integer overflow’s wrapping behavior is considered an error. If you want to wrap explicitly, you can use the standard library type [`Wrapping`](file:///home/zxt/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/doc/rust/html/std/num/struct.Wrapping.html).



### Float

* f64: default, double precision.
* f32: single precision

```rust
let x = 1.1; //64
let y: f32 = 1.1; //f32
```



### Numeric Operations

common senses...... :sleepy:



### Type conversion

```rust
let a: usize = 10;
let b: usize = 11;

//the as operator to force type conversion. 
let c = a as f64 / b as f64;
```



### Boolean

One byte long, either *true* or *false*. Use ``` bool```  to set type:

```rust
let x = true; // implicit typing
let x: bool = false; // explicit typing
```



### Character

Four-bytes long represending Unicode Scalar Value.

```rust
let c = 'A';
let b: char = 'B'
let heart_eyed_cat = '😻'; //how to type this :(
```



## Compound Type

**Tuples** and **Array**

### Tuple

Fixed length, data type of each element varies and can't change after creation.

```rust
    let tup: (u64, i16, bool) = (8, -7, true);
    let (x,y,z) = tup; //destructuring

    println!("x:{},y:{},z:{}", x,y,z); //access by destructuring
    println!("x:{},y:{},z:{}", tup.0, tup.1, tup.2); //direct access

    let mut tup: (u64, i16, bool) = (8, -7, true);
    tup.2 = false; //assign value
	//tup.2='A'; will fail with type mismatch
    println!("x:{},y:{},z:{}", tup.0, tup.1, tup.2); //direct access


```

### Array

Mostly similar to arrays in other language: fixed length and same type.

```rust
    // Arrays
    let arr = [1, 2, 3, 4, 5]; // imply type
    let arr2: [f64; 5] = [1.1, 2.0, 3.0, 4.0, 5.0]; // explicitly set type
    let arr3 = [1; 5]; // set initial value to 1 with five elements
    println!("{},{},{}", arr[0], arr2[1], arr3[2]);

```



## String

Size is variable so is stored in Heap. Thus need to request memory at runtime and free the memory when we're done with it.

```rust
let s = String::from("Hello, World!.")
```



# Functions

Use ```fn``` to define function:

```
fn main() {
    println!("Hello, world!");
    another_function();
    two_numbers(1, 2);
}

fn another_function() {
    println!("Aonther function.");
}

fn two_numbers(x: i32, y: i32) {
    println!("x's value is {}", x);
    println!("y's value is {}", y);
}
```

## Statements and Expressions

> *Statements* are instructions that perform some action and do not return a value. *Expressions* evaluate to a resulting value.

Statements: variable declaration, function definition, value assignments.

```rust
let x = (let y = 6); //Error since "let y=6" is expression won't return any thing.
x = y = 5; //Also wrong since assignment won't return anything.
```

Expression: a umber, calling function, mathematic operation, creating a new scope ```{}```, calling a macro...

```rust
    let y = {
        let x = 3;
        x + 1 // no semicolon
    };
```

***Note***; expressions don't include a semicolon, adding semicolon would turn a expression into a statement.

## Functions with return values

Return type is specified after '->'.

Returned value is synonymously same with the last *expression* or with ```return``` statement explicitly.

```rust
fn sum(x: i32, y:i32) -> i32 {
    x+y // no semicolon
}

fn multiply(x: i32, y: i32) -> i32 {
    return x*y;
    x+y //will never reach this line

}
```



# Control Flow

## ```if```

```rust
    if number < 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }
```

***Note***: The condition after ```if``` must be ```bool``` other wise compilation will fail.

Multiple conditions:

```rust\
    let number = 6;

    if number % 4 == 0 {
        println!("number is divisible by 4");
    } else if number % 3 == 0 {
        println!("number is divisible by 3");
    } else if number % 2 == 0 {
        println!("number is divisible by 2");
    } else {
        println!("number is not divisible by 4, 3, or 2");
    }
```

### ```if``` as expression

if clause is an expression so can be used to assign value:

```\rust
let condition = true
let x = if true {
    5 //no semicolon, other wise will return type ()
} else {
    6 //no semicolon
};//semicolon here
```

***note***: returned types in ```if```, ```else if``` or ```else``` must be compatible, the compiler must know the return type at compile time. The following code will fail:

```rust
    // Example of type mismatch 
	let number = if condition {
        5
    } else {
        "six"
    };
```

## Loops

## ```Loop``` 

The ```loop``` keyword starts a loop.

```rust
let mut counter = 0;
loop {
    if counter >= 10 {
        break;
    }
    counter += 1;
}
```



Loop to return a value: add return value after ```break``` that's used to stop the loop.

```rust
    let mut counter = 0;
    let result = loop {
        if counter >= 10 {
            break counter*2;
        }
        println!("{}", counter);
        counter += 1;
    };
    println!("Result is {}", result);
```



## ```While```

```rust 
fn main() {
    let mut number = 3;

    while number != 0 {
        println!("{}!", number);

        number -= 1;
    }

    println!("LIFTOFF!!!");
}
```



## ```for``` to iterate a collection

```rust
let arr = [1,2,3,4,5];

for element in arr.iter() {
    println!("Current element is {}", element);
}
```

```rust
fn main() {
    for number in (1..4).rev() {  //std::ops::Range
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
    
    // use '_' if you don't care the iteration value.
    for _ in 1..4 {
        println!("iterating");
    }
}
```



```rust
fn main() {
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
```



# Ownership

## Stack and Heap

same thing...

Rust's ownership design is to clean up staled data in Heap without anything like GC.

## Ownership Rules

* Every value in Rust has a variable that's called its **Owner**.
* There could only be **ONE** owner at any time.
* When the owner got out of scope, the value will be dropped.



### Ownership: Move

```rust
let s1 = String::from("Hello, world!"); //Requested memory from heap, s1 is the owner.
let s2 = s1; //s2 is now the owner, and s1 is considered invalid after this line.
// println!("string:{}", s1); Will throw compiler error.
```

For data type stored in Heap, variable assignment like above won't actually copy the data, only the "Pointer" is copied. For String, it's pointer is formed with three values: {pointer: memory address of the first byte; size: size of the string; capacity: total memory returned from system}. So the assignment only copies the above info in **stack** and then invalided s1.

For simple data types(like u32...) that has fixed size in stack, or any type that implements the ```Copy``` trait, assigning won't transfer the ownership but coping the source data into the new variables.

### Clone: the hard way

To actually clone the data, use the ***clone*** method:

```rust
let s1 = String::from("Hello, World!");
let s2 = s1.copy();
println!("s1={}, s2={}", s1, s2);
```

### `Copy` and `Drop` the Trait

`Copy`: keep the old version, like most integers, bool, char, and tuples with fixed-size memory.

```rust
// We can derive a `Copy` implementation. `Clone` is also required, as it's
// a supertrait of `Copy`.
#[derive(Debug, Copy, Clone)]
struct Foo;

let x = Foo;
let y = x;

// `y` is a copy of `x` and x is still valid.
println!("{:?}", x); // A-OK!
```



`Drop`: Old version would lost scope after assignment, like String we just talked about.

An data type can't implement both Copy and Drop trait.

### Ownership and Functions

* Passing values to function argument will pass the ownership, just like variable assignment.
* Returning value to caller will give away ownership.

```rust

fn main() {
	let s = String::from("hello, world.");
    println!("String length is {}", calc_string_len(&s));

    take_ownership(s);
    //println!("s:{}", s); Won't compile, "value borrowed here after move".
    let s = String::from("hello, world.");
    let ss = take_and_giveaway(s);
    println!("ss: {}", ss);
}

fn take_ownership(s: String) {  // Ownership transferred to input argument s.
    println!("s:{}", s);
}

fn take_and_giveaway(s: String) -> String {
    s // Ownership transfered to input argument s and then give away to the returned variable.
}
```



## Ownership: Reference and Borrowing

Refer to a object without taking ownership.

To reference a variable, use symbol ```&```.

```rust
fn main() {
    let s = String:from("Hello, World!");
    let len = calculate_length(&s); // &s to create a reference to s but don't take ownership.
}

fn calculate_length(s: &String) -> usize { // input argument as a reference to String.
    s.len()
} // Here s is out of scope but the string object won't be dropped since s doesn't take ownership of it.

```

Having references in function arguments is called ***borrowing***. 

### Mutable References

Same as variables, references are by default immutable. Errors would be thrown if anyone tries to modify it.

Use ```&mut``` to define mutable references.

The following code won't compile:

```rust
fn main() {
    let s = String::from("hello");

    change(&s);
}

fn change(some_string: &String) {
    some_string.push_str(", world"); // Can't change vlaue on a immutable refernce
}
```

Should use mutable reference instead:

```rust
fn main() {
    let s = String::from("hello");

    change(&mut s);
}

fn change(some_string: &mut String) { //Declaring mutable reference
    some_string.push_str(", world");
}
```

Restrictions on mutable reference

1. The variable your mutable reference points to must be mutable(of course).
2. There could only be **ONE** mutable reference in any scope at any time.
3. **CANNOT** mix the use of immutable and mutable references. i.e: can't not refer to immutable references after a mutable one is declared in the same scope.

```rust
fn multi_ref() {
    let mut s = String::from("Hello, world");
    let s1 = &s;
    let s2 = &s;
    // let s3 = &mut s; // Will fail since we refer to s1, s2 immutable ref after wards.
    println!("s1:{}, s2:{}", s1, s2);

    let s3 = &mut s;  // This is fine since we won't use its immutable version after here.
    println!("s3:{}", s3);
    // println!("s1:{}", s1); //This will cause previous line fail.
}
```



### Dangling References

Rust compiler will prevent dangling references from happening at compile time. This will fail compiling:

```rust
// This won't work!!
fn create_dangling_ref() -> &String {
    let s = String::from("Hello, world");
    return &s; // s will be out of scope after the function, so the reference will point to nowhere, thus a dangling reference. Instead, we could just return s so the returned value will get the ownership of s.
}
```



## Slices

Slice allows you referring to a contiguous sequence of elements in a collection instead of the whole one.

### String Sice

Use symbol ```&str```, use range expression to create a slice of string :

```rust
let s = String::from("hello world");

let hello = &s[0..5];  // first five characters
let world = &s[6..11]; // from index 6(inclusive) to 11(exclusive)
let slice = &s[..2];   // from 0 to index 2(exclusive)
let slice = &s[2..];   // from index 2 to the end
let slice = &s[..];    // all characters, but as a slice
let slice = &s[2..=6]  // from index 2 to index 6(inclusive)
```



Sample to return the first word in a string:

```rust
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
```

The benefit of using slices is it's actually an immutable reference to the string, and would prevent a common bug when the original string is cleared causing the reference invalid.

```rust
fn main() {
    let mut s = String::from("hello world");
    let word = first_word(&s);
    s.clear(); // error!
    println!("the first word is: {}", word);
}
```

```word``` is a immutable reference to (part) of the original string ```s```, and the ```String::clear()``` function requires mutable reference, so would get error here because we're using immutable and mutable references to the same object interleavingly.

### Array Slice

```rust
let a = [1,2,3,4,5];
let slice = &a[1..3]; //crate an array slice
```

It's type is ```&[i32]``` or what ever other data type.

There would also be slices for other collections which we would talk later.



# Structure

## Basics

To define a structure:

```rust
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```

Order doesn't matter here.

To create a structure instance:

```rust
let user: User = User {
        username: String::from("user1"),
        email: String::from("someemail@rust.com"),
        active: true,
        sign_in_account: 1234,
    };
```

Use ```.``` to access fields of a struct:

```rust
println!("User's email: {}", user.email);
```

To update a struct's field, use ```.``` as well, but the variable must be mutable:

```rust
let mut user: User = User {
    ...
}
user.email = String::from("someotheremail.rust.com");
```

Note that the whole struct instance must be mutable, rust doesn't support partial mutable struct.



## Some struct shorthand

### Struct Init Shorthand

If variable's name is the same as the struct field's name:

```rust
fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}
```



### Struct Update Syntax

When constructing a struct from another instance and only update some of its fields:

```rust
let user2 = User {
    email: String::from("another@example.com"),
    username: String::from("anotherusername567"),
    ..user1
};
```



### Tuple Struct

Basically when you don't care the fields name in your struct, or simply want to name a kind of Tuple.

```rust
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);

let num = black.0; // access fields in tuple struct.
```

Note that ```Color```  and ```Point``` are different types even though they both formed with 3 i32 variables. 



### Unit-Like Trait

A struct with no field. 



## Ownership of Struct

The ```User``` example would take ownership for all its field values, this could be just bad. It's possible to assign a reference of each value but would need to specify ```lifetime``` parameter(will cover this later).

All methods are defined inside ```impl``` blocks, and multiple ```impl``` blocks are allowed.

## An Example

```rust
struct Rectangle {
    width: u32,
    height: u32,
}

fn main() {
    let rect1 = Rectangle { width: 30, height: 50 };

    println!(
        "The area of the rectangle is {} square pixels.",
        area(&rect1)
    );
}

fn area(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}
```



## Method

Methods are almost identical to function, except that it can only be declared within the context of struct, trait, or enum, and its first parameter is always ```&self``` (or ```&mut self``` depending on whether you want to modify it or not) which is a reference to the instance that this methods is called on.

```rust
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}

fn main() {
    let rect1 = Rectangle { width: 30, height: 50 };

    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );
}
```

### Associated Functions

Functions inside an ```impl``` block that doesn't take ```self``` as first parameter, for example```String::from()```.

```rust

impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle { width: size, height: size }
    }
}

let area = Rectangle::square(10);
```



# Enum

### Most Basics

Allows defining a type by enumerating its possible values.

```rust
enum IPAddrKind {
    V4,
    V6,
}

let four= IPAddrKind::V4;
```

Can also be used as input type:

```rust
fn route(ipAddrKind: IPAddrKind) {}
```

### Associating with values

Defining a enum type with variants is very similar to defining a struct except using keywork ```enum``` 

```rust
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String)
}
let some_ipv4_addr = IpAddr::V4(10, 0, 0, 0);
let some_ipv6_addr = IpAddr::V6(String::from("some ipv6..."))
```

Advantage of this approach is we can associate different numbers and types of associated data for each variant of the enum type, you can put any kind of data like String, Number or struct into an enum variant.A

Yet another way to define enum variants:

```rust
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

impl Message {
    fn call(&self) {
        // method body would be defined here
    }
}

// Using Message
fn tstFunc() {
    let msg1 = Message::Quit;
    let msg2 = Message::Move{x:1, y:2};
    let msg3 = Message::Write(String::from("abd"));
    
    msg3.call();
}
```

You can create and use enum variants in the exact same way as struct, including declaring associated methods.

### The ```Option``` Enum

Encoding a simple scenario when a value could have some value or nothing. Rust do not have ```null```, but it's encoded in the ```Option``` enum:

```rust
enum Option<T> {
    Some<T>,
    None,
}

let some_integer = Some(14); // variable's type is inferred by compiler.
let some_string = Some("some_string");
let absent_number: Option<i32> = None; // Need to specify the variable's type because the compiler can't infer its type when it's calue is Option::None
```

```Option<T>``` has two variants ```Some<T>``` and ```None``` which are both included in the prelude, this means we don't needs to use ```Option::``` prefix to use it. Check ```Option```'s full spec in ```rustup doc --std``` under ```std::option::Option```.



## The ```Match``` Operator

### The Basics

A powerful **expression** in Rust that allows comparing a value against several patterns and execute code based on which pattern matches( Will talk about patterns and matching in detail in the future):

```rust
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter,
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => 1,
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter => 25,
    }
}
```

It's formed with the ```match``` keyword followed by the variable to be compared. Match operator could have several arms with each formed with the pattern and ***expression*** to be executed when its pattern is matched.

### Pattern that Binds to Values

A way to extract values out of enum variants.

```rust
#[derive(Debug)] // so we can inspect the state in a minute
enum Message {
    Quit,
    Move{x: i32, y: i32},
    Call(String)
}

fn inspect_message(msg: Message) {
    match msg {
        Message::Quit => {
            println!("It's quit. {:?}", msg);
        },
        Message::Move{x, y} => {  // short for "Message::Move{x:x, y:y}"
            println!("It's a Move. Move coordination x:{}, y:{}", x, y);
        },
        Message::Call(s) => {
            println!("It's Call, the content is {}", s);
        }

    }
}
```

### Matching with ```Option<T>```

```match``` is a friend of ```enum```.

```rust
#![allow(unused_variables)]
fn main() {
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        None => None,
        Some(i) => Some(i + 1),
    }
}

let five = Some(5);
let six = plus_one(five);
let none = plus_one(None);
}
```

### ```match``` is Exhaustive

The ```match``` operator is exhaustive:  we must exhaust every last possibility in order for the code to be valid.

Use ```_``` placeholder to cover remaining cases:

```rust

#![allow(unused_variables)]
fn main() {
let some_u8_value = 0u8;
match some_u8_value {
    1 => println!("one"),
    3 => println!("three"),
    5 => println!("five"),
    7 => println!("seven"),
    _ => (),
}
}
```



## The ```if let``` Statement

An alternative way of writing ```enum``` expression when you only care about one matching case. For example, the following code written with ```match``` can be simplified with ```if let```:

```rust
let some_u8_value = Some(0u8);
match some_u8_value {
    Some(3) => println!("three"),
    _ => println!("not three"),
}

//Re-written as if let:
if let Some(3) = some_u8_value {
    println!("three");
} else {
    println!("not three");
}
```

**Note**: ```if let``` statement don't have the exhaustive feature as match, so it's only good when you only care about one scenario and prefer to ignore all other possible ones.



# Package, Crates, and Modules

* Package: A create feature that lets you create, test , and share crates. One package can have multiple binary crates and optionally one library crate, but at least contain one create either binary or library.
* Crate: A tree of module that produces a library or executable.
* Module and Use: Let you control the organization, scope and privacy of paths.
* Paths: ways of naming an item like function, struct and enums...

## Packages and Crates

In side a package, file ```src/main.rs``` is the root of a binary create with the same name as the package, file ```src/lib.rs``` is the root of a library crate with the same name as the package. You can group multiple binary crates by putting them under folder ```src/bin```.

*Modules* let us organize code within a crate into groups for readability and easy reuse. Modules also control the *privacy* of items, which is whether an item can be used by outside code (*public*) or is an internal implementation detail and not available for outside use (*private*).

To create a module use the keyword ```mod```: 

```rust
mod front_of_house {
    mod hosting {
        fn add_to_waitlist() {}

        fn seat_at_table() {}
    }

    mod serving {
        fn take_order() {}

        fn serve_order() {}

        fn take_payment() {}
    }
}
```

Modules can be nested, like module front_of_house is the parent of both module hosting and serving. We can put functions, structs, and enums...etc into modules.

The module tree of the above modules would be something like this:

```
crate
 └── front_of_house
     ├── hosting
     │   ├── add_to_waitlist
     │   └── seat_at_table
     └── serving
         ├── take_order
         ├── serve_order
         └── take_payment
```

Note that ```crate``` is the default root module, it would be used in a absolute path of an item.

## Paths

The way to find an item in a module tree. It can take two forms:

- An *absolute path* starts from a crate root by using a crate name or a literal `crate`.
- A *relative path* starts from the current module and uses `self`, `super`, or an identifier in the current module.

### Accessibility and Privacy Boundry

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();
}
```

**Notes**:

1. Modules (including child modules) and all other items(function, struct, enum, constants, methods) are private by default, need to add the ```pub``` keyword to make it public accessible.
2.  Items in a parent module can’t use the private items inside child modules, but items in child modules can use the items in their ancestor modules. 
3. The ```front_of_house``` module is not public but still accessible by ```eat_at_restaurant``` because they're on the same module.

### Relative path with keyword ```super```

Just like ```..``` in linux file system path.

```rust
fn serve_order() {}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::serve_order();
    }

    fn cook_order() {}
}
```

### ```pub``` with Structs and Enums

#### Struct

If a struct is marked ```pub``` this doesn't mean its fields are public as well, fields should be marked public case-by-case:

src/lib.rs file:

```rust
mod back_of_house {
    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast {
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }
}

pub fn eat_at_restaurant() {
    // Order a breakfast in the summer with Rye toast
    let mut meal = back_of_house::Breakfast::summer("Rye");
    // Change our mind about what bread we'd like
    meal.toast = String::from("Wheat");
    println!("I'd like {} toast please", meal.toast);

    // The next line won't compile if we uncomment it; we're not allowed
    // to see or modify the seasonal fruit that comes with the meal
    // meal.seasonal_fruit = String::from("blueberries");
}
```

#### Enum

Different from struct, if a enum is marked ```pub``` then all its variants would be public.



### Bring Paths into Scope with ```use```

Like symbolic link in linux file system:

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

use crate::front_of_house::hosting; //with absolute path
use self::front_of_house::hosting; // with relative path, needs tostart with 'self'

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}

use std::collections::HashMap;

fn main() {
    let mut map = HashMap::new();
    map.insert(1, 2);
}
```

The idiomatic way to use path is to use parent module for functions(so we know which module this function belongs to) and use path to the item itself for struc, enums. (This is just convension.)

**Note**: rust won't allow bringing two items with the same name into the scope.

### The ```as``` keyword.

Specifying a nick name for the items brought into the scope.

```rust
use std::fmt::Result;
use std::io::Result as IoResult;

fn function1() -> Result {
    // --snip--
}

fn function2() -> IoResult<()> {
    // --snip--
}
```

### Re-exporting with ```pub use```

When we bring a name into scope with the `use` keyword, the name available in the new scope is private. To enable the code that calls our code to refer to that name as if it had been defined in that code’s scope, we can combine `pub` and `use`.

```rust
mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}
    }
}

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
```

External modules who brought this module into scope can also call ```hosting::add_to_waitlist()```.

### Using External Packages

As documented in Chapter 2, add dependencies in cargo.toml file:

```rust
[dependencies]
rand = "0.5.5"
```



### Nested Paths

```rust
use std::{cmp::Ordering, io};
// equivalent to
//use std::io;
//use std::cmp::Ordering;

use std::io::{self, Write};
// equivalent to
// use std::io;
// use std::io.Write;

use std::collections::*;//global import that brings every public items into scope.
```



## Separating Modules into Multiple Files

When module goes complex, it would be easier to navigate among the code if we separate modules into several files.

file: ***src/lib.rs***:

```rust
mod front_of_house; //Only declaring the module, implementations is in src/front_of_house.rs. This is telling the compiler to find the implementation of this module from a file with the same name.

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}
```

file: ***src/front_of_house.rs***

```rust
pub mod hosting {
    pub fn add_to_waitlist() {}
}
```



# Common Collections: Vector, String, and HashMap

## Vector

Put multiple values in a single data structure that puts all values next to each other. It's implemented in Generic and since we won't know its size at compile time all values are stored in heap. All elements in a vector must be of the same type.

### Create

To create a vector:

```rust
let v: Vec<i32> = Vec::new();  // when not creating with initial value, needs to specify the type of variable.

let v = vec![1,2,3,4,5];  // create with initial values, vec! is a macro for convenience that create a vector with the given initial values. Rust will infer the type of v so don't need to specify its type here.
```

### Update

```rust
let mut v = Vec::new();
v.push(1);
v.push(2);
v.push(3);
```

Use the ```push``` method to add elements to the end of the vector. Note that the vector **must be mutable** so we can make changes to it. Also note that we still don't need to specify the type of ```v``` since we're only adding the same type to the list and the compiler is smart enough to infer the vector's type. 

### Dropping a Vector

Same as all other variables, if a vector got out of scope then it would be cleaned up. Once a vector is cleaned up, all the values it holds would also be cleared.

### Reading

```rust
let v: vec![1,2,3,4,5];

let third: &i32 = &v[2];
match v.get(2) {
    Some(value) => println!("The third value is {}", value),
    None => println!("There is no third value.")
}
```

Basically two ways to read vector's value by index:

1. Use ```&``` and ```[]``` which would return a reference to the object ```&T```, and would throw error if the index is out of bound.
2. The get method which returns type Option<T>. And would return Option::None if the index is out of bound. This forces the caller to check both scenarios.

When we get a reference to a element in a vector in any of the two ways, rust will check the validity of the references. Remember that we can't have both mutable and immutable reference to a object, so the following code would fail compiling:

```rust
let mut v = vec![1, 2, 3, 4, 5];

let first = &v[0];

v.push(6);

println!("The first element is: {}", first);
```

The line creating reference ```first```  would fail because we already have a mutable ref to the vector. Why? Because since ```v``` is mutable we can increase or delete elements in the vector, which means the elements in the vector could be re-located or deleted with those operations, thus having a immutable ref to its element at the same time would be unsafe.

### Iterating a vector

Read only:

```rust
let v = vec![100, 32, 57];
for i in &v {
    println!("{}", i);
}
```

Updates:

```rust
let mut v = vec![100, 32, 57];
for i in &mut v {
    *i += 50;
}
```

Note: the line ```*i  += 50;``` is dereference operator to get its value.

### Using Enum to store different types

When want to have different types in a vector, consider using Enum to "wrap" different types of elements.

```rust
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

let row = vec![
    SpreadsheetCell::Int(3),
    SpreadsheetCell::Text(String::from("blue")),
    SpreadsheetCell::Float(10.12),
];
```

### Other useful Vector functions

```pub fn len(&self) -> usize``` : The size of the vector.

```pub fn clear(&mut self)```:  Clear the vector.

```pub fn pop(&mut self) -> Option<T>```:  Get and remove the last element in the vector.

```pub fn insert(&mut self, index: usize, element: T)```: Insert the element at designated index.

```pub fn remove(&mut self, index: usize) -> T```:  Remove and return element at designated index, panics if index is out of bound.

```pub fn append(&mut self, other: &mut Vec<T> )```: Move all elements from ```other``` into ```self``` and cleanup ```other```.

```pub fn reverse(&self)```: reverse the order of items in place.

```pub fn sort(&mut self)```: stable  sort with O(nlogn) complexity.



## String

The ```String``` type is actually provided by rust's std library rather than the language core(that would be the string slice ```str```). Encoded by [UTF-8](https://en.wikipedia.org/wiki/UTF-8). 

### Creation

```rust
let mut s = String::new(); // Creating empty string where we can load data into later.
let s = "abcdefg".to_string(); // Creating a string object with initial data.
let s = String::from("abcdefg"); // Equivalent to the above line.
```

Note: the ```to_string``` function can be used on any type that implements ```Display``` trait( will talk about trait later), like the string literal does.

### Update

#### ```push_str``` and ```push```

* ```push_str``` : take a string slice and append it to the string:

  ```rust
  let mut s1 = String::from("foo");
  let s2 = "bar";
  s1.push_str(s2);
  println!("s2 is {}", s2); //since push_str take string slice, so s2 still holds the onwership.
  ```

* ```push```: append a single character:

  ```rust
  let mut s = String::from("lo");
  s.push('l');
  ```

#### Concatenation and format

```rust
let s1 = String::from("Hello, ");
let s2 = String::from("world!");
let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used
```

Note that s1 would be invalid after the concatenation because the ```+``` operator use string's add method defined as ```fn add(self, s: &str) -> String```. Because the first parameter is ```self```  rather than a borrowing reference ```&self``` so it would take its ownership to the add function and then discard it. The output s3 would be the concatenation of s1 and s2.

To format a string:

```rust
let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");

let s = format!("{}-{}-{}", s1, s2, s3);
```

Note that ```format!``` is a macro.



### String Indexing

In short: not allowed.

```rust
let s1 = String::from("hello");
let h = s1[0];   //won't compile
```

Will cause following compile error:

```
error[E0277]: the trait bound `std::string::String: std::ops::Index<{integer}>` is not satisfied
 -->
  |
3 |     let h = s1[0];
  |             ^^^^^ the type `std::string::String` cannot be indexed by `{integer}`
  |
  = help: the trait `std::ops::Index<{integer}>` is not implemented for `std::string::String`
```

The reason is ```String``` in rust is basically a wrapper of Vec<u8>, and it use UTF-8 encoding which is a variable width character encoding(each grapheme will take from 1 to 4 bytes) to represent. Using index to access kth byte could cause bug when only part of the character is returned. So rust compiler just banned such operation to prevent potential bugs. 

```rust
let hello = "Здравствуйте";
let s = &hello[0..4];
let s = &hello[0..5];//will cause panic!
```

Since hello is formed wity Cyrillic characters which consists two bytes, so the slice of first 5 characters would fail because the last letter is only half of a letter:```'byte index 5 is not a char boundary; it is inside 'р' (bytes 4..6) of `Здравствуйте`'```.

***Be cautious when slicing strings that have non-ascii encoded characters***.



### Iterating over String

```rust
for c in "नमस्ते".chars() {
    println!("{}", c);
}
```

You can also iterating its bytes but might make no sense:

```rust
for b in "नमस्ते".bytes() {
    println!("{}", b);
}
```



## HashMap

Storing key-value pairs in the heap. It's homogeneous: all keys must have the same type and all values must have the same type.

Use cryptographically strong hashing algorithm which trades of speed for security.

### Creation

```rust
use std::collections::HashMap;    // HashMap is not part of the prelude

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10); // types will be infered.
scores.insert(String::from("Yellow"), 50);
```

Or create with ```collect``` method on a vector of tuples:

```rust
use std::collections::HashMap;

let teams  = vec![String::from("Blue"), String::from("Yellow")];
let initial_scores = vec![10, 50];

let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
```

Note the type annotation `HashMap<_, _>` is needed here because it’s possible to `collect` into many different data structures and Rust doesn’t know which you want unless you specify. For the parameters for the key and value types, however, we use underscores, and Rust can infer the types that the hash map contains based on the types of the data in the vectors.

### Ownership

For types implemented ```copy``` trait, its value would be copied when added to the hashmap. Otherwise hashmap will take the ownership and initial variable won't be valid anymore.

```rust
use std::collections::HashMap;

let field_name = String::from("Favorite color");
let field_value = String::from("Blue");

let mut map = HashMap::new();
map.insert(field_name, field_value);
// field_name and field_value are invalid at this point, try using them and
// see what compiler error you get!
```

When inserting a referent to hash map, the value the reference points to won't be copied, but need to make sure the value would remain valid for at least as long as the hash map is valid. Will cover this in detail in ```lifetime``` part.

### Read

Use the ```get``` method which returns ```Option<&V>```.

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

let team_name = String::from("Blue");
if let Some(score) = scores.get(&team_name) {
    println!("Score is {}", score);
}
```

Or iterate thru the map:

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

for (key, value) in &scores {
    println!("{}: {}", key, value);
}
```

### Update

#### Override

Insert twice to the same key would override the previous value:

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Blue"), 25);

println!("{:?}", scores);
```

#### Update only key doesn't exist

Use ```entry```  function to inspect if a key exists in hash map and take actions accordingly.

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();
scores.insert(String::from("Blue"), 10);

scores.entry(String::from("Yellow")).or_insert(50);
scores.entry(String::from("Blue")).or_insert(50);

println!("{:?}", scores);
```

The entry function checks if the key you specified exists or not and return a enum ```Entry``` which is defined as:

```rust
/// A view into a single entry in a map, which may either be vacant or occupied.
///
/// This `enum` is constructed from the [`entry`] method on [`HashMap`].
///
/// [`HashMap`]: struct.HashMap.html
/// [`entry`]: struct.HashMap.html#method.entry
#[stable(feature = "rust1", since = "1.0.0")]
pub enum Entry<'a, K: 'a, V: 'a> {
    /// An occupied entry.
    #[stable(feature = "rust1", since = "1.0.0")]
    Occupied(#[stable(feature = "rust1", since = "1.0.0")] OccupiedEntry<'a, K, V>),

    /// A vacant entry.
    #[stable(feature = "rust1", since = "1.0.0")]
    Vacant(#[stable(feature = "rust1", since = "1.0.0")] VacantEntry<'a, K, V>),
}
```

the ```or_insert``` function will insert the value if the key doesn't exist(entry function returns Entry::Vacant) and returns a mutable reference to the value(so if you can update it as you want).

#### Update based on previous value

As indicated in previous session, use the returned mutable ref to update the key:

```rust
use std::collections::HashMap;

let text = "hello world wonderful world";

let mut map = HashMap::new();

for word in text.split_whitespace() {
    let count = map.entry(word).or_insert(0);
    *count += 1;  // remember to de-reference.
}

println!("{:?}", map);
```





# Errors

Errors are part of software's lifecycle, so rust requires the acknowledgements of possible errors before it could even compile.

Rust don't have concept like Exception, errors are represented in two ways:

* Recoverable represented with type ```Result<T, E>```.
* Unrecoverable thrown by macro ```panic!```.

## Unrecoverable ```panic!```.

Will print a error message, unwind and cleanup the stack, and then quit.

```rust
fn main () {
    panic!("crash and burn")
}
```

Use the stacktrace to debug the error.



## Recoverables with ```Result```.

```Result``` is defined as:

```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

To open a file  handle errors during the open:

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => panic!("Problem opening the file: {:?}", other_error),
        },
    };
}
```

Both ```Result```'s variants ```OK``` and ```Err``` are part of the prelude so there is no need to explicitly import them.

The ```Ok``` arm simply return the file descriptor if open succeeded. The ```Err``` arm exams the error kind and do proper operations respectively. 

Too many ```match``` statements? The ```Result``` enum has many functions that takes closure(like lambda, will cover later) to make codes cleaner:

```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create("hello.txt").unwrap_or_else(|error| {
                panic!("Problem creating the file: {:?}", error);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });
}
```



### Shortcuts to panic on error: ```unwrap``` and ```expect```.

* ```unwrap``` returns the value in ```OK``` variants or throw the Error in ```Err``` variant.

  ```rust
  let f = File::open("hello.txt").unwrap();
  ```

* ```expect``` is similar to ```unwrap``` except it allows customizing error message for easier debugging.

  ```rust
  let f = File::open("hello.txt").expect("Failed to open hello.txt");
  ```

### Error Prepagation

Instead of handling possible errors, returning the error to the caller function and let it decide what to do.

```rust
use std::io;
use std::io::Read;
use std::fs::File;

fn read_username_from_file() -> Result<String, io::Error> {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}
```

This can be shortened with the ```?``` operator:

```rust
use std::io;
use std::io::Read;
use std::fs::File;

fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}
```

or even shorter:

```rust
fn read_username_from_file() -> Result<String, io::Error> {
    let mut s = String::new();
    File::open("hello.txt")?.read_to_string(&mut s)?;
    Ok(s)
}
```

The `?` placed after a `Result` value is defined to work in almost the same way as the `match` expressions we defined to handle the `Result` values in Listing 9-6. If the value of the `Result` is an `Ok`, the value inside the `Ok` will get returned from this expression, and the program will continue. If the value is an `Err`, the `Err` will be returned from the whole function as if we had used the `return` keyword so the error value gets propagated to the calling code.

**Note**: The ```?``` operator can only be used in functions that return ```Result``` type.

## To ```panic!``` or not to ```panic!```

### Cases when panic is preferred:

* Testing & prototyping: to uncover potential errors earlier.

* When we have more information than the compiler:

  ```rust
  let home: IpAddr = "127.0.0.1".parse().unwrap();
  ```

  We know that hard coded "127.0.0.1" is a legal ip address.

* Any cases when code could end up in a **bad state**. A bad state means certain assumptions, guarantee, contract or invariant has been broken. 
  Notes about bad state:

  * The bad state is not something that's ***expected*** to happen occasionally.
  * Your code after this points needs to rely on not being in this bad state.
  * There's not a good way to encode this information in the types you use.

  If someone called your code and passed in values that don't make sense, the best choice is to ```panic``` and alert the programmer there is a bug in their code.(Man std library would panic if passed in invalid input).
  If calling external code that is out of your control and it returns an invalid state that you have no way to recover, then panic.

### Cases when ```Result``` is preferred.

Any case when we expect certain error could happen occasionally, like http error, and we could recover from such errors.

### Use Custom Types for Input Validation.

Assuming we are expecting input value between 1 to 100:

```rust

pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess {
            value
        }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}
```

This forces caller to use ```Guess::new``` function to create a object, thus enforce value range checking and would panic if it's not within the expected range.



# Generic Types, Traits, and Lifetimes

## Generic Types

To reduce duplicate codes.

### In Functionss

Format: 

```rust
fn largest<T>(list:&[T]) -> T {......}
```

Sample usage:

```rust
fn largest<T:PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }
    largest
}

fn main() {
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest(&number_list);
    println!("The largest number is {}", result);

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest(&char_list);
    println!("The largest char is {}", result);
}
```

Type parameter is placed in the bracket between function name and parameters.

Here to make code compile T has to implement ```PartialOrd``` and ```Copy``` trait. ```PartialOrd``` trait is needed because we need compare elements in the array to find the largest, and ```Copy``` is required because we eventually need to return a copy instead of a reference, so items there must be copiable.

### In Struct Definitions

format:

```rust
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let integer = Point {x: 5, y: 10};
    let float = Point {x: 1.0, y: 4.0};
}
```

Note:

1. The type of ```T``` will be inferred when constructing them.
2. Since both x and y are o the same generic type T, so they must be of the same type.



### In Enum Definition

format:

```rust
enum Option<T> {
    Some(T),
    None
}
```



### In Method Definition

We can use generics to implement methods for enum and structs.

format:

```rust
struct Point<T> {
    x: T,
    y: T,
}

// Implement all generic type T.
impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

// Only implement type f32.
impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

fn main() {
    let p = Point { x: 5, y: 10 };

    println!("p.x = {}", p.x());
}
```

Note: 

1. We have to declare ```T``` right after ```impl``` keyword to declare the generic type ```T```.

2. We can also choose to only implement type with certain types.

Also note that generic type parameters in a struct definition aren't always the same as those you use in that struct's method sgnature:

```rust
struct Point<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point<T, U> {
    fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
        Point {
            x: self.x,
            y: other.y,
        }
    }
}

fn main() {
    let p1 = Point { x: 5, y: 10.4 };
    let p2 = Point { x: "Hello", y: 'c'};

    let p3 = p1.mixup(p2);

    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
}
```

In the method ```mixup``` ```T``` and ```U``` are declared after ```impl``` keyword which means they go with the struct definition, while ```V``` and ```W``` are declared after function name which means they go with the function.

### Performance of Code Using Generic

No additional costs.

Rust accomplishes this by performing **monomorphization** which would iterate thru all code using generic types and turn all of them into concrete types.



## Traits: Defining Shared Behavior

Equivalent to ***interfaces*** in java.

### Definition

```rust
pub trait Summary {
    fn summarize(&self) -> String;
}
```

Implementing a trait:

```rust
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tewat {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweat {
    fn summarize(&self) -> String {
        format!("{} : {}", self.username, self.content)
    }
}
```

**Note**: When implementing a trait on a type, make sure either the trait or the type must be local to our current crate. On other words, we can't implement external traits for external types. This is to ensure a property of program called ***coherence*** so that other people's code can't break your code and vice versa.

 

### Default Implementations

```rust
pub trait Summary {
    fn summarize(&self) -> String {
        String::from("Read more...")
    }
}
```

For a type to use this default implementation:

```rust
impl Summary for NewsArticle  {}
```

***Note:*** Default implementations can call other methods in the same trait, even if those other methods don't have a default implementation.



### Traits as Parameters

Traits can be used as parameters to accept many different types.

```rust
pub fn notify(item: impl Summary) {
    println!("Breaking news! {}", item.summarize());
}
```

This function accepts any types that implements the ```Summary``` trait.

The above format is actually a short cut for **Trait Bound Syntax** to reduce boiler plate code:

```rust
pub fn notify<T: Summary>(item: T) {
    println!("Breaking news! {}", item.summarize());
}
```

#### For multiple trait bounds

1. Use the ```+``` symbol:

```rust
pub fn notify(item: impl Summary + Display) {...}
```

Or in complete form:

```rust
pub fn notify<T: Summary + Display>(itme: T) {...}
```

2. The **where** clause:

```rust
fn some_function<T, U>(t: T, u: U) -> i32
	where 	T: Display + Clone,
			U: Clone + Debug
{...}
```



### Returning Types that Implement Traits

We can use the ```impl Traits```  syntax in the return position to return a value of some type that implements a trait. This is extremely useful in the context of ***closures and iterators***.

```rust
fn returns_summarizable() -> impl Summary {
    Tweat {
        .....
    }
}
```

**Note**: you can only use ```imple Traits``` syntax if you're returning a single type. The following example won't work:

```rust
fn return_summarizable(switch: bool) -> impl Summary {
    if switch {
        NewsArticle {
            ...
        }
    } else {
        Twitter {
            ...
        }
    }
}
```



### Using Trait Bounds to Conditionally Implement Methods

We can conditionally implement methods for types that implements specific traits:

```rust
use std::fmt::Display;

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self {
            x,
            y,
        }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x > self.y {
            println!("x>y");
        } else {
            println!("y>=x");
        }
    }
}
```

In above example, the ```cmp_display``` function is only implemented for types that implemented the Display and PartialOrd traits.

We can also conditionally implement traits based on the traits the type implements:

```rust
impl<T: Display> ToString for T {
    ......
}
```

### Associated Types: Specifying Placeholder Types in Trait Definitions

**Associated types** connect a type placeholder with a trait such that the trait method definitions can use these placeholder types in their signature. Implementing a trait with a associated type requires specifying the concrete type for the placeholder type. A good example is Rust's ```std::iter::Iterator```:

```rust
pub trait Iterator {
    type Item;
    
    fn next(&mut self) -> Option<Self::Item>;
}
```

An example to implement ```Iterator```:

```rust
impl Iterator for Counter {
    type Item = u32;  //Specifying the concrete type for the associated type.
    
    fn next(&mut self) -> Option<Self::Item> {
        //.....
    }
}
```

Associated type is very similar to generic types, so why we need associated type?

When a trait has a generic parameter, it can be implemented for a type multiple times, changing the concrete types of the generic type parameter each time. When we use the ```next``` method on ```Counter``` we would have to provide type annotations to indicate which implementation of Iterator we want to use.

With associated type, however, we don't need to annotate types because we can't implement a trait on a type multiple times.

### Default Generic Type Parameters and Operator Overloading

Rust supports default concrete type for the generic type. It is mostly useful when 1) extending a type without breaking  existing code and 2) allowing customization in specific cases most users won't need. A good example is ***Operator overloading*** where we can override some operators specified under ```std::ops``` by implementing related traits:

```rust
use std::ops::Add;

#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

impl Add for Point {     //Using default type which is Self
    type Output = Point;
    
    fn add(self, other: Point) -> Point {
        Point {
            x: self.x + other.x,
	            y: self.y + other.y,
        }
    }
}
```

```std::ops::Add``` is defined as:

```rust
trait Add<RHS=Self> {    //Default type is Self.
    type Output;
    fn add(self, rhs: RHS) -> Self::Output;
}
```

### Fully Qualified Syntax

```<Type as Trait>::function(receiver_if_method, next_args, ...);```

Fields can be ignored if rust can infer from the context.

### Trait Dependency

Sometimes you need another trait's function when implementing a trait. The trait you're relying on is a ***super trait*** to the trait you're implementing.

```rust
use std::fmt;

trait OutlinePrint: fmt::Display {   //Specifying that fmt::Display is a super trait of the trait we're implementing
    fn outline_print(&self);
}
```

This requires that type implemented OutlinePrint must also implement std::fmt otherwise the compile will fail.



## Validating References with Lifetimes

The main aim of lifetime is to prevent dangling references.

### Borrow Checker

Rust compiler has a borrow checker that compares scopes to determine whether all borrows are valid.

The following code won't compile because reference ```r``` went out of scope in the ```println``` macro.

```rust
{
    let r;                // ---------+-- 'a
                          //          |
    {                     //          |
        let x = 5;        // -+-- 'b  |
        r = &x;           //  |       |
    }                     // -+       |
                          //          |
    println!("r: {}", r); //          |
}  
```



### Lifetime Annotation

Lifetime annotation is required when the rust compiler can't tell the lifetime of the returned reference.

```rust
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

In the above code, the compiler can't infer the lifetime of the returned reference who could be either ```x``` or ```y```, thus the borrow checker can't tell whether some value could be out of scope or not.

Lifetime annotations describe the relationships of the lifetimes of multiple references to each other without affecting the lifetimes.

```rust
&i32				// a reference
&'a i32				// a reference with an explicit lifetime
&'a mut i32			// a mutable reference with an explicit lifetime.
```



### Lifetime Annotation in Function Signatures

```rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

The function signature tells Rust compiler that for some lifetime ```'a``` the functions takes two parameters,  both of which lives at least as long as lifetime ```'a```, and the returned value will also at least live as long as lifetime ```'a```. ```'a``` will be the overlapped lifetime of the two inputs. But note again that this won't change parameter's actual lifetime, we just need to provide inputs and return values lifetime so the compiler's borrow checker could reject any values that don't adhere to these constraints.



### Thinking in Terms of Lifetimes

The way in which we need to specify lifetime parameters depends on what the function is doing. We only need to provide lifetime parameter when the compiler can't infer some lifetimes. 

If we change the ```longest``` function in the following way then we don't need to specify the lifetime:

```rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    x
}
```

Since the return value's lifetime would be identical to the input parameter's so compiler would be able to imply return value's lifetime, thus the above code compiles without lifetime parameters.



When returning a reference from a function, the lifetime parameter for the return type needs to match the lifetime parameter for one of the parameters. Because if not, then it must refer to some object created inside the function, which would create a dangling reference when the function returned.

```rust
// Won't compile
fn longest<'a>(x: &str, y: &str) -> &'a str {
    let result = String::from("really long string");
    result.as_str()
}
```

When needs to return some object created inside the function, return the owned data type instead of a reference so the calling function will be responsible for managing and cleaning up.



### Lifetime Annotations in Struct Definitions

It's possible to have references in struct's fields instead of owned data types, but that would require a lifetime annotation in every such fields.

```rust
struct ImportantExcerpt<'a> {
    part: &'a str,
}

fn main() {
    let novel = String::from("blablabla.blablabla");
    let first_sentense = novel.split('.')
    	.next()
    	.expect("Could not find a '.'");
    let i = ImportantExcerpt{part: first_sentense};
}
```

Lifetime annotation is very the same in syntax as generic types in struct. This lifetime annotation here means an instance of ```ImportantExcerpt``` can't outlive the reference it holds in its fields.



### Lifetime Elision

There are **three** ***lifetime elision rules*** where we won't need to specify lifetime parameters because compiler could infer them.

The compiler will apply the above three rules and will throw compile error if it still can't infer the lifetimes. In such case you should explicitly add lifetime annotations.

1. Each parameter that's a reference gets its own lifetime parameters.
   for example ```fn foo(a: &str, b: &str)``` will be applied as ```fn foo<'a, 'b>(x: &'a str, y: &'b str)```.
2. If there is exactly one input lifetime parameter, that lifetime is assigned to all output lifetime parameters: ```fn foo<'a>(x: &'a str) -> &'a i32```
3. If there are multiple input lifetime parameters, but one of them is ```&self``` or ```&mut self``` because it's a method, the lifetime of ```self``` is assigned to all output lifetime parameters.

As a result of these there rules, the following function is valid:

```rust
fn first-world(s: &str) -> &str; //apply rule 1 and 2
```

And this one is invalid:

```rust
fn longest(x: &str, y: &str) -> &str;
// applying rules 1
fn longest<'a,'b>(x: &'a str, y: &'b str) -> &str // still can't apply the output since rule 2 and 3 is not satisfied.
```



### Lifetime Annotations in Method Definitions

Use the same syntax as generic types:

```rust
impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        3
    }
}
```

Note: The lifetime parameter declaration after ```impl``` and its use after the type name are required, but we're not required to annotate the lifetime of the reference to ```self``` because of the first elision rule.



### The Static Lifetime

One special lifetime is ```'static``` which means the reference can live for the entire duration of the program, like string literals:

```rust
let s: &'static str = "I have a static lifetime.";
```



# Tests

## How To

Use ```#[test] ```attributes to mark a function as a test function.

```rust
#[cfg(test)]				// Misterious as of now.
mod tests {					// Create a inner module for testing
    #[test] 				// Test attribute
    fn it_works() {
        assert_eq!(2+2, 4); // Macro to assert two values are equal.
    }
}
```

Command ```cargo test``` will execute the above test:

```
zxt@zxt-manjaro:~/ws/rust_learn/unit-test
( ͡° ͜ʖ ͡°)->cargo test
   Compiling unit-test v0.1.0 (/home/zxt/ws/rust_learn/unit-test)
    Finished dev [unoptimized + debuginfo] target(s) in 0.39s
     Running target/debug/deps/unit_test-71f0522f8d28caaf

running 1 test
test tests::it_works ... ok


test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out

   Doc-tests unit-test

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out

```

The output is strait-forward enough except following sections:

1. ```0 measured;```: Rust supports benchmark tests where we measure software's performance, mostly useful for nightly builds. Check rust documentation for details about benchmarking tests.
2. ```0 filtered out```. We can optionally filter which tests should run and which shouldn't. Check next section for details.
3. ```Doc-tests```. For documentation tests, which ensures codes and docs are in sync. Will cover in later chapters.

### Assert Macros

* ```assert!``` : asserting the single argument is true. Optional error message as string.

  ```rust
  #[derive(Debug)]
  struct Rectangle {
      width: u32,
      height: u32,
  }
  
  impl Rectangle {
      fn can_hold(&self, other: &Rectangle) -> bool {
          self.width > other.width && self.height > other.height
      }
  }
  
  #[cfg(test)]
  mod tests {
      use super::*;
  
      #[test]
      fn larger_can_hold_smaller() {
          let larger = Rectangle { width: 8, height: 6 };
          let smaller = Rectangle { width: 6, height: 4 };
  
          assert!(larger.can_hold(&smaller));
          assert!(!smaller.can_hold(&larger));
      }
  }
  ```

* ```assert_eq!``` and ```assert_ne!```: Asserting if the two arguments are equal/not equal. Optional error message as string.

  ```rust
  pub fn add_two(a: i32) -> i32 {
      a + 2
  }
  
  #[cfg(test)]
  mod tests {
      use super::*;
  
      #[test]
      fn it_adds_two() {
          assert_eq!(4, add_two(2));
      }
  }
  ```

Note that both ```assert_eq!``` and ```assert_ne!``` use operator ```==``` and ```!=``` respectively, and when tests failed it use ```Debug``` formatting to print the error message. Thus, the compared objects should implement ```PartialEq``` and ```Debug``` traits for it to blindly work. It's straightforward to add the ```#[derive(PartialEq, Debug)]``` attributes to annotate your struct or enum definition to fix this, or manually implement the ```std::fmt::Debug``` trait.

### Checking Panics with ```#[should_panic]```

Similar to ```expects(Exception.class)``` in junit, to ensure at certain scenarios the test target should panic.

```rust
fn panic() {
    panic!("Don't panic!");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "Don't panic!")]
    fn test_panics() {
        panic();
    }
}
```

There is a optional argument ```expected=``` which is a harness to make sure that the failure message contains the provided text.



### Using ```Result<(), E>``` in Tests

Instead of using assert macros, we can alternatively return ```Result<(), E>``` to indicate whether test passed or failed:

```rust
fn inspect_even(num :u32) -> Result<u32, String> {
    if num%2 == 0 {
        Result::Ok(num)
    } else {
        Result::Err(String::from("Not a even number"))
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("two plus two does not equal four"))
        }
    }
    
    #[test]
    fn test_even_with_result() -> Result<(), String> {
        inspect_even(4)?;
        Ok(())
    }
    
    #[test] // Will fail.
    fn test_even_with_result() -> Result<(), String> {
        inspect_even(3)?;
        Ok(())
    }
}
```

It's will be handy to use ```?``` operator to propagate ```Result::Err``` variant returned by inner function calls to mark the test as failure.

***Note*** that We can't use ```#[should_panic]``` annotation on tests that return ```Result<T, E>```. 

***Note*** that it must return ```Result<(), E>``` instead of other types for ```T```, as only this implements the ```std::process::Termination``` trait: https://doc.rust-lang.org/std/process/trait.Termination.html. `()` is an empty tuple, a simple zero-sized type (it uses no memory) with only one value possible, `()`. It’s also known as the *unit type*. Its use in a return type of `Result<(), E>` means “if nothing goes wrong, there’s no further value produced”. The semantics are what’s important—the call was OK.



 ## Control Test Runs

``` cargo test``` compiles your code in text mode and run the resulting test binaries. There are two types of parameter in ```cargo test```: those go to ```cargo test``` and those go to the test binaries. They are separated by symbol ```--``` as: ```cargo test {test_args} -- {binary_args}```. For example, ```cargo test --help``` print help messages for ```cargo test``` itself and ```cargo test -- --help``` print help messages for the test binary.

### Parallel or Consecutive

By default rust run tests in parallel threads to accelerate tst speed. However, this could be problematic if your tests have "shared states" like environment variables or file systems. You can add ```--test-threads=#``` as test binary argument to limit the number of threads being used for testing.

```shell
cargo test -- --test-threads=1
```

This would use single thread and tests would run consecutively.

### Showing Function Output

By default cargo captures all std outputs if test passed, and only show them when test failed. We can optionally use ```--nocapture``` flag to display all std output:

```shell
cargo test -- --nocapture --test-thread=1
# Use single thread as well otherwise outputs will be interleaved.
```

### Running a Subset of Tests by Name

* Single Test
  Pass the test function name after ```cargo test```:

  ```shell
  cargo test {test_name}
  ```

* Multiple Tests
  Same as above, run all tests whose name contains the provided feature:

  ```shell
  cargo test {name_feature}
  ```

* Ignore some tests
  Add ```#[ignore]``` attribute to ignore certain tests. To run the ignored tests, use ```--ignored``` test binary argument:

  ```shell
  cargo test -- --ignored
  ```

  

## Test Organization

In rust world tests are categorized as unit tests and integration tests.

### Unit Tests

* The ```#[cfg(test)]``` attribute tells the compiler that only compile the following module in test mode. Thus these code won't be compiled when running ```cargo build``` to save time.
  In the attribute, the ```cfg``` stands for ***configuration*** and tells Rust the the following item should only be included given a certain configuration option(in the above case "test").
* Testing Private Functions: some languages like java don't even allow testing private function, and there is even a debate whether it's necessary to test private functions. But anyway it's possible to test private items(just as all the above codes).

### Integration Tests

Integration tests are tests for, well, integrating of your software's components.

* The _tests_ directory next to _src_ is where Rust recognizes as places to find integration tests.
  If there are tests under _tests_ then ```cargo test``` output will show three kind of test results: unit tests, doc tests, and then integration tests.
  You can still run a single or multiple specific tests by passing their name(s) as test arguments.
  To run all tests in a particular test file, add the ```--test``` argument to ```cargo test```:

  ```shell
  cargo test --test {TEST_FILE_NAME_WITHOUT_.rs}
  ```

* Submodules in Integration Tests.

  We can move some common helper functions which would be shared among multiple tests into ```{PATH}/mod.rs```. The file name ```mod.rs``` is recognized by Rust as a mod file instead of test files. And we can then import the mod file in other tests to reuse its functionalities:
  _file tests/common/mod.rs_:

  ```rust
  pub fn setup_tests() {...}
  ```

  _file tests/integration_tests.rs:

  ```rust
  mod common;
  
  #[test]
  fn it_works() {
      common::setup_tests();
      assert_eq!(1+1, 2);
  }
  ```

* Integration Tests for Binary Crates
  All we mentioned in above are for library crates not for binary crates. We can't create tests in _tests_ directory to bring functions defined in _src/main.rs_ with ```use``` statement as binary crates are supposed to run on their own. 
  Usually we put simple logic in _src/mian.rs_ and put most logis under _src/lib.rs_ file so that they can be tested.







# Building a Command Line Program

Review previous sections and build a command line  tool ```miniprep``` that implements some functions of ```grep```.

## Accepting Command Line Arguments

Use ```std::env::args``` in the standard library which returns the arguments this program is started with:

```rust
use std::env;
	let args: Vec<String> = env::args().collect();
```

The ```args()``` function returns object of type ```Args``` which implements ```Iterator``` trait, thus we can use its ```collect()``` method to turn it into collections.

***Note*** that the fist argument would be the executable itself.

## Reading Files

Using ```std::fs::read_to_string``` function:

```rust
use std::fs;

let contents = fs::read_to_string(filename).expect("Read Error.");
```

Note: this function returns type ```std::io::Result<T> ``` which is defined as ```std::result::Result<T, std::io::Error>```.

## Improving Modularity and Error Handling

At this point of time our ```minigrep``` has a single source file and can read a particular file:

```rust
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 3 {
        panic!("Didn't receive enough arguments.")
    }

    let query = &args[1];
    let filename = &args[2];

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading this file.");

    println!("Received arguments: {:?}", args);
    println!("Searching for {} in file {}", query, filename);
    println!("Content read {}", contents);

```

Areas to improve:

* Separate logics into multiple functions.
* Less variables to maintain for every function.
* Better error handling.
* Checking input validity.

### Separation of Concerns for Binary Projects

* Split program into main.rs and lib.rs.
* Complex logics move into lib.rs.

The ```main.rs``` should be limited doing the following things:

* Calling cli parsing logics.
* Setting up configurations.
* Calling functions in lib.rs.
* Handling errors from lib.rs.

After concern separation:

```rust
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 3 {
        panic!("Didn't receive enough arguments.")
    }

    let config = Config::new(&args);

    let contents = fs::read_to_string(config.filename)
        .expect("Something went wrong reading this file.");

    println!("Content read {}", contents);
}

struct Config {
    query: String,
    filename: String,
}

impl Config {
    fn new(args: &[String]) -> Config {
        let query = args[1].clone();
        let filename = args[2].clone();

        Config { query, filename }
    }
}
```

The major changes:

* Move configurations from string tuple into a customized struct ```Config```. This is to improve flexibility as it would be much easier to add/modify configuration fields in the future.
* Moved configuration parsing logic into ```Config```'s method for better cohesion. 
  * Note that in the ```Config::new``` function we use ```clone``` method to copy the arguments' content, this is not ideal since it would increase memory prints. But in such a small program this really simplifies the code coz we no longer need to worry about its ownership and lifecycle. There are definitely better ways doing this and we would cover this in later chapter.

### Better Error Handling

Should do better than just panic.

```rust
use std::env;
use std::fs;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    let contents = fs::read_to_string(config.filename)
        .expect("Something went wrong reading this file.");

    println!("Content read {}", contents);
}

struct Config {
    query: String,
    filename: String,
}

impl Config {
    fn new(args: &[String]) -> Result<Config, &'static str> {

        if args.len() < 3 {
            return Err("Didn't receive enough arguments.");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        Ok(Config { query, filename })
    }
}
```

Major changes:

* Return ```Result``` instead of panic on error.
* Note that the return type is ```Result<Config, &'static str>```. The static lifetime annotation is to ensure the error message encoded has static lifetime, which would always be safe to access.
* In ```main``` function we use ```Result```'s method ```unwrap_or_else``` function which returns the result on success and trigger a closure on error instead of simply panic.

### Extracting Logics from Main

After extraction:

src/main.rs:

```rust
use std::env;
use std::process;
use minigrep::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    // Using if let, equivalent to unwrap_or_else
    if let Err(e) = minigrep::run(config) {
        println!("Application error: {}", e);
        process::exit(1);
    }
}
```

src/lib.rs

```rust
use std::error::Error;
use std::fs;

pub struct Config {
    pub query: String,
    pub filename: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {

        if args.len() < 3 {
            return Err("Didn't receive enough arguments.");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        Ok(Config { query, filename })
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {

    let contents = fs::read_to_string(config.filename)?;
    println!("Content read {}", contents);
    Ok(())
}
```

Notes:

1. Moving ```Config```, ```run``` into ```lib.rs``` file, and have to make them public so they can be accessed from external crates.
2. Moved logics searching pattern to ```run``` function in ```lib.rs```. It returns type ```Result<(), Box<dyn Error>>```. ```()``` is the unit type that it return on success and the ```Box``` generic is when error happened, this is a ***trait object*** that we would cover in later sections.



## Developing the Library's Functionality with Test-Driven development

Test driven development(TDD):

1. Write a test that fails and run it to make sure it fails for the reason you expect.
2. Write or modify just enough code to make the new test pass.
3. Refactor the code you just added or changed and make sure the tests continue to pass.
4. Repeat from step 1!



## Working With Environment Variables

To check environment variable, use ```std::env::var``` method in the standard library. It takes a string literal as environment's name, and returns ```Result``` object with the ```Ok``` variant indicating the variable is set and contains its value, and ```Err``` indicates it's not set.

```rust
        let case_sensitive = env::var("CASE_SENSITIVE").is_err();
```

Checkout more functions in ```std::env``` in manual.



## Writing Error Message to Standard Error Instead of Standard Output

The ```println!``` macro only prints to stdout, sometimes we want to put error messages into stderr.

The standard library provides the ```eprintln!``` macro that prints to the standard error stream.



# Closures and Iterators: Functional Programing Features

## Closures

### Syntax:

```rust
let some_closure = |num| { num+1 };
```

Starts with vertical pipes and inside which we specify closure's parameters, then follows with curly brackets that holds the body. There is an option to get rid of the curly bracket if the body is a single expression. Note that there should be a semicolon to end the ```let``` statement.

Calling a closure is exactly like calling a function:

```rust
let increment = some_closure(10);
```

### Type Inference and Annotation

Type annotations are required for function definition because they're part of an explicit interface exposed to your users. However, closures, as anonymous functions, are relevantly only within a narrow context, thus compiler is reliably able to infer the types of the parameters and return type. But, of course, you can still add type annotations.

```rust
fn	add_one_v1		(x: u32) -> u32 {x + 1}
let add_one_v2  = 	|x: u32| -> u32 {x + 1};
let add_one_v2  =	|x|				{x + 1};
let add_one_v3	= 	|x|				 x + 1 ;
```



### Storing Closures and it's Traits

All closures implement at least one of the following traits: ```std::ops::Fn```, ```std::ops::FnMut```, and ```std::ops::FnOnce```. The differences between them are at how they allow capturing environment variables.

To use these traits at generic type bounding, we need to add types to the ```Fn``` traits to represent the types of the parameters and return values the closures must have to match this trait bound.

```rust
fn call_with_one<F>(func: F) -> usize
    where F: Fn(usize) -> usize {
    func(1)
}
```



### Capturing Environment Variables

Closures have an additional capability that functions don't have: capturing their environment and access variables from the scope in which they're defined.

```rust
fn main() {
    let x = 4;
    let equal_to_x = |z| z== x;
    let y = 4
    assert!(equal_to_x(y));
}
```

There are three ways closures capture environment variables, which maps to the three ways function take parameters as well the three traits for closures(```Fn```, ```FnOnce```, and ```FnMut```.): taking ownership, borrowing mutably and borrowing immutably.

* ```FnOnce```: Takes the ownership of the environment variables. The ```Once``` part came from that since it took the ownership so it can only be called once since a variable can't be taken with ownership for more than once.
* ```FnMut```: can change the environment variable thru mutably borrows.
* ```Fn```: borrows value from the environment immutably.

Note that ```Fn``` extends ```FnMut``` and ```FnMut``` extends ```FnOnce```.

When you create a closure, rust infers which trait to use based on how the closure uses the values from the environment. Closures that don't ```move``` the captured variables also implement ```FnMut``` , and closures don't need mutable access also implement ```Fn```.

Use the ```move``` keyword to take ownership of the captured parameter. This is useful when passing a closure to a new thread to move the data so it's owned by the new thread.

```rust
fn main() {
    let x = vec![1, 2, 3];
    let equal_to_x = move |z| z == x;
    // println!("can't use x here: {:?}", x);
    let y = vec![1,2,3];
    assert!(equal_to_x(y));
}
```



## Iterators

Similar concepts to iterators in other languages.

Iterators are lazy, which means it won't take any effect until you call methods that consume the iterator to use it up.

### Definition

```rust
pub trait Iterator {
    type Item;
    
    fn next(&mut self) -> Option<Self::Item>;
    
    ...
}
```

There is only one method ```next``` which returns the next element and consume the iterator.

Note that the ```type Item``` line is defining an **associated type** with this trait, which means implementing the ```iterator``` trait requires that you also define an ```Item``` type, and the ```Item``` type is the return type of the ```next``` method.

Using iterator:

```rust
fn iterator_demostration() {
    let v1 = vec![1, 2, 3];
    let mut v1_iter = v1.iter(); // Note that the iterator object must be mustable other wise we can't call the next function which chanes the iterator's state.
    assert_eq!(v1_iter.next(), Some(&1));
    assert_eq!(v1_iter.next(), Some(&2));
    assert_eq!(v1_iter.next(), Some(&3));
    assert_eq!(v1_iter.next(), None;
}
```

Note that iterator returned by ```Vec::iter()``` function is over **immutable references**. To iterate over mutable references we should use ```iter_mut``` function, to take ownership and return owned values we should use ```into_iter``` function.

### Iterator's method that consumes Iterator

There are series of methods in ```iterator``` that are written using the ```next``` function which would consume the iterator, they're thus called **consuming adapters**. For example, the sum function:

```rust
let v1 = vec![1,2,3];
let v1_iter = v1.iter();
let total: u32 = v1_iter.sum();
```

### Iterator's methods that produces other Iterators.

Methods that allow you to change iterators into different kinds of iterators are called **iterator adaptors**. (Like the Stream apis in java.). For example the ```map``` function:

```rust
let v1: Vec<i32> = vec![1,2,3];
let v2 = v1.iter().map(|x| x+1).collect();
```

### Using Iterators that Capture Their Environment

Using the ```filter``` iterator adaptor which takes a closure that evaluates each element and return a boolean object. If it returns true the element will be included in the new generated iterator.

```rust
#[drive(PartialEq, Debug)]
struct Shoe {
    size: u32,
    stype: String,
}

fn shoes_in_my_size(shoes: Vec<Shoe>, shoe_size: u32) -> Vec<Shoe> {
    shoes.into_iter().			// Using into_iter to take ownership
    	.filter(|x| x.size == shoe_size)
    	.collect()
}
```

Since the ```shoes_in_my_size``` took ownership of a vector of shoes, we should use the ```into_iter``` to get the vector's ownership and pass to the returned vector.



# Smart Pointers

A pointer is a general concept for a variable that contains an address in memory. The **reference** in rust is a kind of pointer though they don't have any capability other than referring to other data.

**Smart pointer**, on the other hand, are data structures that not only act like a pointer but also have additional metadata and capacities. For example, it enables **reference counting** which allows multiple owner of a data and cleanup the data once no one owns it.

Reference is a kind of pointer that don't take ownership, and on the other hand smart pointers own the data they point to.

 Smart pointers are usually implemented with structs. It contains these two traits:

* ```Deref```: allows the smart pointer struct to behave like a reference so that you can write code to deal with either reference or smart pointer.
* ```Drop```: allows writing code to be executed when the instance the smart pointer pointing to went out of scope.

## ```Box<T>```

Allows storing data in heap instead of stack. What's remained in stack is only a pointer to the heap data.

We may use ```Box<T>``` in the following scenarios:

* When we have a type whose size can't be known at compile time. (If you want to put data of a type in stack, compiler must be able to know its size at compile time.)
* When you have large amount of data and you want to transfer the ownership of them without coping them.
* Using ```trait object```.

```Box<T>``` can be used as a reference(implementing ```Deref```) and deallocate spaces in heap when the object goes out of scope(implementing ```Drop```).



## The ```Deref``` Trait

### Treating Smart Pointers Like Regular References with the ```Deref``` Trait

Implementing the ```Deref``` trait can customize the behavior for the dereference operator ```*```. 

 Following the Pointer to the Value with the Dereference Operator

```rust
let x = 5;
let y = &x; // y is now a reference to x

assert_eq!(5, x);
assert_eq!(5, *y); // Derefernce to the value it points to
//assert_eq!(5, y); // Will get error "no implementation for `{integer} == &{integer}`"
```

 Using ```Box<T>``` *LIKE* a Reference

```rust
let x = 5
let y = Box::new(5);
let z = &y;

assert_eq!(5, x);
assert_eq!(5, *y); //Using Box **LIKE** a reference.
assert_eq!(5, *y.deref()); //Equivalent to above, will cover detail in Deref.
assert_eq!(5, **z); // Need two dereference operator: 1st to convert reference to Box to a Box value and next dereference along the Box's pointer to the value it points to.
```

We can use the dereference operator to follow the box's pointer in the same way that we did when y was a reference.

### Implementing the ```Deref``` Trait

The ```std::ops::Deref``` trait has one single function ```deref``` that borrows ```self``` and returns a reference to the inner data. 

```rust
struct MyBox<T>(T); // Defining a tuple struct

impl <T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T; // Associated type for the Deref trait to use, it's a slightly differnt way of declaring a generic parameter, we will cover this later.

    fn deref(&self) -> &T {
        &self.0
    }
}
```

Note: the ```deref``` function returns a reference to the inner type(```&T```) instead of a value of it. This is necessary because otherwise we will transfer the ownership out of the struct.

To play with ```MyBox```:

```rust
    let n = MyBox::new(String::from("Hello, Rust."));
    println!("{}", *n); // *n's type is &str
    println!("{}", *(n.deref())); //Equivalent to above line.
```

Note: when we do ```*n``` in the above example and what rust actually do is ```*(n.deref())```. The ```Deref``` trait tells rust what to do when it's being dereferenced.

#### Deref Coercions

**Deref coercion** is a convenience that Rust performs on arguments to functions and methods. Deref coercion converts a **reference to a type** that implements ```Deref``` into a reference to a type that ```Deref``` can can convert the original type into. Deref coercion happens automatically when we pass a reference to a particular type's value as an argument to a function or a method that doesn't match the parameter type in the function or method signature. A sequence of calls to the ```deref``` method converts the type we provided into the type the parameter needs.

```rust
fn hello(m: &str) {
    println!("{}", m);
}

    // Test for MyBox
    // Note that String implements Deref<Target=str>
    let m = MyBox::new(String::from("Hello, world!"));
    hello(&m);
    hello(m.deref());
    hello(&(*m)[..]);
    hello(&(*m.deref())[..]);
    hello(&(*m));
```

When the ```deref``` trait is defined for the the types involved, Rust will analyze the types and use ```Deref::deref``` as many time as necessary to get a reference to match the parameter's type. The number of times is calculated at compile time so there is no time penalty.

#### Deref Coercion and Mutability

Similarly, you can implement the ```DerefMut``` trait to override the ```*``` operator on mutable references. Their relationship:

* From ```&T``` to ```&U``` when ```T: Deref<Target=U>```.
* From ```&mut T``` to ```&mut U``` when ```T: DerefMut<Target=U>```.
* From ```&mut T``` to ```&U``` when ```T: Deref<Target=U>```.

Note that rust will also coerce a mutable referene to an immutable one. But the reverse is impossible.



### Running Code on Cleanup with the ```drop``` Trait

Customizing what happened when a value is about to go out of scope.

```rust
struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Cleaning up CustomSmartPointer with data {}", self.data)
    }
}
```

Note: the ```std::ops::Drop``` is included in the prelude, so we don't need to bring it to scope.

Note: variables are automatically dropped at the reverse order of their creation.

Rust don't allow calling the ```Drop```'s ```drop``` method explicitly as it rust will still call this function at the variable's going out of scope, which could lead to double cleanup error. Thus Rust don't alllow us calling the ```Drop```'s ```drop``` function explicitly. We can also use the ```std::mem::drop``` function to cleanup memory before the variable's destruction.

```rust
fn main() {
    let c = CustomSmartPointer { data: Stirng::from("some data")};
    drop(c);
}
```

Note that ```std::mem::drop``` function is also part of the prelude.

Also note that the ```drop``` function take **value** as input not reference, so Rust's ownership model will ensure we won't cleanup the memory when some other reference still points to it.



## Reference Counting Smart Pointer: ```Rc<T>```

We use ```Rc<T>``` when we want to allocate some data on the heap for multiple parts of our program to read and we can't determine at compile time which part will finish using the data last. If we can, we could just the last user's as the owner and normal ownership model would just work.

Every time we call ```Rc::clone```, the reference count to the data will increase, and the data won't be cleaned up unless there are zero references to it.

```rust
enum List {
    Cons(i32, Rc<List>),
    Nil,
}

use crate::List::{Cons, Nil};
use std::rc::Rc;

fn main() {
    let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
    println!("count after creating a = {}", Rc::strong_count(&a));
    let b = Cons(3, Rc::clone(&a));
    println!("count after creating b = {}", Rc::strong_count(&a));
    {
        let c = Cons(4, Rc::clone(&a));
        println!("count after creating c = {}", Rc::strong_count(&a));
    }
    println!("count after c out of scope = {}", Rc::strong_count(&a));
}

```

Note: ```Rc<T>``` only allows immutable references for the reference counting to work.

Note: we prefer ```Rc::clone(&a)``` rather than ```a.clone()``` so we can visually distinguish between the deep-copy kinds of clone and the kinds of clones that increases the reference counting.

Also, every time we call the clone method the reference count is increased, and it would decrease when the value goes out of scope.



## ```RefCell<T> ``` and the Interior Mutability Pattern

Interior mutability is a design pattern in Rust that allows you to mutate data even when there are only immutable references to the data. This is usually not allowed, thus it use unsafe code inside a data structure to bend Rust's usual rules.

Rewind that rust only allows either one mutable references or any number of immutable references at any given time. With regular references and ```Box<T>```, the borrowing rules' invariants are enforced at **compile time** while, in ```RefCell<T>``` these are enforced at **runtime**. When you break the rule with ```RefCell<T>``` rust will panic and exit.

The reason we need interior mutability is that static analysis, like rust compiler, is inherently conservative, meaning it will trigger an compile error when it can't be sure whether the code will comply with the ownership rule. This is necessary because we want the compiler to be 100% reliable. In cases when static analysis is impossible we would rely on runtime analysis, even though it may cause runtime performance penalties.

### ```RefCell<T>```

One example use case is mocking a dependency with a immutable reference:

```rust
// Trait to mock
pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct Tracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
}

impl<'a, T> Tracker<'a, T>
	where T: Messenger {
    pub fn new(messenger: &T) -> Tracker<T> {
        Tracker {
            messenger,
            value: 0,
        }
    }
        
    pub fn set_value(&mut self, value: usize) {
        self.value = value;
        self.messenger.send("Tracking message.");
    }
}
```

The test code:

```rust
#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger { sent_messages: RefCell::new(vec![]) }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.borrow_mut().push(String::from(message));
        }
    }

    #[test]
    fn it_sends_an_over_75_percent_warning_message() {
        // --snip--

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}
```

The trick here is the ```Tracker``` struct keeps a immutable reference to a ```Messenger``` trait to send notifications. To test ```Tracker``` is sending notifications correctly we created ```MockMessenger``` which stores all sent message in a Vector. However, this against the mutability because ```Tracker``` only keeps immutable reference to it and we need mutable reference to store all sent messages. And we don't want to change the immutable reference to ```Messenger``` to mutable just for testing purpose.

Here is when ```RefCell``` came to play. 

With ```RefCell<T>``` we use ```borrow``` and ```borrow_mut``` methods which returns smart pointers of type ```Ref<T>``` and ```RefMut<T>``` respectively to represent immutable and mutable references. ```RefCell``` tracks how many mutable and immutable references has been checkout and would panic if we broke the ownership model(more than one mutable and mutable immutable at same time). 



# Fearless Concurrency

## Using Threads

Rust uses 1:1 threading, which means one thread in Rust maps directly to one thread in the underlying OS. Some other languages use M:N threads which means they have their own thread implementation which might not necessary might to a underlying OS threads. 

```rust
    use std::thread;
	use std::time::Duration;

	let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("Hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(100));
        }
    });
    for i in 1..10 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(100));
    }

    handle.join().unwrap();

    let v = vec![1,2,3];

    // Need to use move keyword since we won't know the lifespan of v and the thread.
    let handle = thread::spawn( move || {
        println!("Here is the vector: {:?}", v);
    });

    handle.join().unwrap();
    //println!("Can't access v anymore from the main thread. {:?}", v);
```

Most concepts similar to java or python except the ```move``` keyword. The ```move``` keyword forces the closure to take ownership of the valuesit's using in the body. As a result, we can't use these variables in the main thread any more.



## Message Passing Between Threads

Similar to Go's slogan: "Do not communicate by sharing memory; instead, share memory by communicating". Rust implemented ***channel*** for threads to accomplish concurrency by sending messages rather than sharing data.

A channel has two ends: a transmitter and a receiver. A channel is said to be closed if either the transmitter or the receiver is dropped.

```rust
use std::thread;
use std::time::Duration;
use std::sync::mpsc;

fn play_with_channel() {
    // Using channel
    let (tx, rx) = mpsc::channel();
    // Copy the tx so we can send to the receiver from multiple sources.
    let tx1 = mpsc::Sender::clone(&tx);

    // The spawned thread needs to own the transmitter to be able to send messages.
    thread::spawn(move || {
        let vals = vec![
            String::from("Hello"),
            String::from("From"),
            String::from("The"),
            String::from("Spawned"),
            String::from("Thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
        //println!("Can't access val anymore {}", val);
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("Tx1"),
            String::from("Said"),
            String::from("Hi"),
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }

    });

    // let received = rx.recv().unwrap()
    // rx only out of iteration when the channel is broken.
    for received in rx {
        println!("Received value: {}.", received);
    }

}
```

Channel is implemented in ```std::sync::mpsc``` crate where mpsc means ***multiple producer single consumer*** and you get the point that it allows multiple threads to send message to a single thread.

A few notes:

1. The thread closure needs to own the transmitting end to be able to send messages thru the channel. Thus we need the ```move``` keyword to pass the ownership.
2. The receiver has two major APIs: ```recv()``` and ```try_recv()```. ```recv()``` would block until message received and returns ```Result<T,E>``` to encapsulate normal results and errors. ```try_recv``` returns immediately and would get error if message is not yet available.
3. You can also has your thread listen on the receiver by iterating thru the receiver ```for received in rx {...}```. The iteration won't stop until the channel is dropped.
4. Use ```mpsc::Sender::clone(&tx)``` to copy the sender so we can send message from multiple threads.



## Shared State Concurrency

Message passing is a fine way to handle concurrency, but not the only way. Channels in any programming language are similar to **single ownership** while shared memory concurrency is like **multiple ownership**. Typically sharing memory could be risky but Rust's ownership model has made it much easier and safer.

```rust
use std::thread;
use std::sync::{Mutex, Arc};

fn play_with_mutex() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("The final counting is {}", *counter.lock().unwrap());
}
```

Notes:

1. The type ```Mutex``` is the mutex lock similar to implementations in other languages. When calling ```lock()``` function it will return a smart pointer of type ```MutexGuard``` wrapped into type ```LockResult``` that we handled with the call to ```unwrap```. ```MutexGuard``` implements ```Deref``` trait and that's why we can access its underlying value with deref operator```*num += 1;```. It also implemented ```Drop``` trait thus it will be unlocked when it goes out of scope.
2. The type ```Arc<T>``` means "Atomic Reference Counting" is similar to type ```Rc<T>``` but is thread safe. It is used to support multiple ownership among threads.(```Rc<T>``` is not thread safe when incrementing and decrementing reference counts, thus can't be used in multi-thread scenarios).
   Similar to ```Rc<T>```, ```Arc::clone(&counter)``` increments the reference counting, and will decrement when it goes out of scope.
3. Note that the mutex reference ```counter``` is immutable but we can get a mutable reference to its underlying data, this means ```Mutex``` provides interior mutability, as the ```Cell``` family does.

### Arc

Shared references in Rust disallow mutation by default, and `Arc` is no exception: you cannot generally obtain a mutable reference to something inside an `Arc`. If you need to mutate through an `Arc`, use [`Mutex`](https://doc.rust-lang.org/std/sync/struct.Mutex.html), [`RwLock`](https://doc.rust-lang.org/std/sync/struct.RwLock.html), or one of the [`Atomic`](https://doc.rust-lang.org/std/sync/atomic/index.html) types.

```Arc<T>``` implements ```Sync``` and ```Send``` as long as ```T``` implements them. The reason is ```Arc<T>``` makes it threadsafe to have multiple ownership of the same data, but doesn't add thread safety to its own data. To make it thread safe it's common to pair with other thread safe types, like ```Arc<Mutex<T>>```.



## Extensible Concurrency with the ```Sync``` and ```Send``` Traits.

Rust as a language don't have much feature for concurrency, most concurrency features are implemented in std library. But it does embed two concepts in the language: the ```std::marker``` traits ```Sync``` and ```Send```.

### ```Send```: Allowing ownership transference between threads

The ```Send``` trait tells the compiler that this object's ownership can be transferred among threads. Almost all rust type is ```Send```. One common exception is ```Rc<T>``` where its reference counter is not thread safe. As a result, when you're trying to transfer a ```Rc<T>```'s ownership to other threads the compiler will give you a error. This is a protection mechanism to avoid non-thread safe type to be shared in multiple threads.

### ```Sync```: Allowing access from multiple threads

It indicates whether it's safe for the type to be referenced from multiple threads. Similar to ```Send``` most rust types are also ```Sync``` , one common exception is again ```Rc<T>```.

### Avoid Manually Implementing ```Send``` and ```Sync```.

If a type is composed entirely of types that are ```Sync``` or ```Send```, then the composed type also automatically implemented the traits. Manually implementing ```Send``` or ```Sync``` will require unsafe rust code, so don't do this unless you're 100% sure what you are doing.



# OO With rust

Check source code.



# Patterns and Matching

Patterns are a special syntax in Rust to match against the structure of types, both complex and simple. It gives control over a program's control flow.

## Places Control Can Be Used

### ```match``` arms

As is discussed before:

```rust
match VALUE {
    PATTERN1 => EXPRESSION1,
    PATTERN2 => EXPRESSION2,
    PATTERN3 => EXPRESSION3,
    _ => EXPRESSION4,
}
```

One requirements for the expression arm is that it must be **exhaustive**. It's convenient to use ```_``` to catch all remaining options but it will also ignore variables captured from the value;

### ```if let``` statement

A shorter way to write ```match``` expression when we only care about one option. it can be combined with other statements like ```else```, ```if else```, and even ```else if let``` such that you can evaluate a different argument and pattern. This would make it more flexible than ```match``` statements.

```rust
    let color: Option<&str> = None;
    let is_tuesday = false;
    let age: Result<u8, _> = "29".parse();
    
    if let Some(col) = color {
        println!("Got the color: {}", col);
    } else if is_tuesday {
        println!("It's Tuesday!.");
    } else if let Ok(age) = age {
        println!("My age is {}", age);
    } else {
        println!("Didn't catch anything...");
    }
```

One downside of ```if let``` statement is that it doesn't check exhaustiveness, which means we could have bugs by ignoring certain corner cases.

### ```while let``` conditional loops

Similar to ```if let``` statement, ```while let``` allows the loop to continue until the pattern is mismatched:

```rust
    let mut v = vec![1,2,3];
    while let Some(n) = v.pop() {
        println!("{}", n);
    }
```

### ```for``` loop

```rust
    let v = vec![1,2,3,4,5];
    for (index, value) in v.iter().enumerate() {
        println!("Value at index {} is {}", index, value);
    }
```

### ```let``` statement

Yeah, you're right, the ```let``` statement also uses patterns. Actually the form of let statement is:

```
let PATTERN = Expression;
```

As a simple example, expression ```let x = 5```  has a pattern ```x``` which means "bind what matches in the expression to the variable x". Since x is the whole pattern, it effectively means "bind everything to the value x whatever its value is.

Another example ```let (x, y, z) = (1, 2, 3)``` pattern is three element pattern x, y, z and would be bound with value in the expression with 1,2,3.

### Function Parameter

Just like ```let``` statement, function parameters are also patterns.

```rust
fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Received coordinates: {}:{}", x, y);
}
```



## Refutability of Pattern

Determines whether a pattern matching could be failed or not.

Function parameters, ```let``` and ```for``` statements only accept irrefutable pattern because it can't do anything when the pattern failed matching. On the other hand, ```if let```  and ```while let``` requires refutable pattern because it will be a redundant if it's irrefutable(technically it's allowed, your compiler will just warn you).



## Pattern Syntax

### Matching Literals

Simplest case.

```rust
let x = 5;
match x {
    1 => println!("One"),
    _ => println!("Not One"),
}
```



### Matching Named Variables

Named variables are irrefutable patterns that it will match any value. Note that ```match``` keyword will start a new scope so the variable in the pattern will shadow the variable with the same name outside the scope.

```rust
    let a = Some(5);
    let b = 10;
    
    match a {
        Some(10) => println!("Ten"),
        Some(b) => println!("Matched {}", b), //b's value is not 10.
        _ => println!("Nothing matched.")
    }
    println!("a={:?}, b={:?}", a, b);
```



### Multiple Patterns

We can have multiple patterns in match's arm:

```rust
    let x = 1;
    match x {
        1|2 => println!("one or two"),
        _ => println!("Other values.")
    }
```



### Matching Ranges with ```..=``` 

```rust
let x = 5;

match x {
    1..=5 => println!("one through five"),
    _ => println!("something else"),
}
```

Note that range operators only applies to **numeric and char** types.

### Destructing to Break Apart Values

```rust
struct Point {
    x: i32,
    y: i32
}

fn main() {
    let p: Point = Point{x: 1, y: 2};
    
    let Point{x:a, y:b} = p; // struct's fields destructed to variable a and b.
    println!("x:{}, y:{}", a, b); 
    let Point{x,y} = p; //shortcut when matching named variables are same with field names.
    println!("x:{}, y:{}", x, y);
    
    match p {
        Point{x, y:10} => println!("Matches y=10"), //matches as long as y=10
        Point{x:1, y} => println!("Matches x=1"), //matches as long as x=1
        Point{x: a, y: b} => println!("x: {}, y: {}", a, b), // remaining
    }
}
```

### Destructing Enums

```rust
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

fn main() {
    let msg = Message::ChangeColor(0, 160, 255);

    match msg {
        Message::Quit => {
            println!("The Quit variant has no data to destructure.")
        }
        Message::Move { x, y } => {
            println!(
                "Move in the x direction {} and in the y direction {}",
                x,
                y
            );
        }
        Message::Write(text) => println!("Text message: {}", text),
        Message::ChangeColor(r, g, b) => {
            println!(
                "Change the color to red {}, green {}, and blue {}",
                r,
                g,
                b
            )
        }
    }
}
```



### Ignoring Patterns

#### Usage of ```_```

We've seen using ```_``` to ignore the whole or part of a value in a pattern:

```rust
if let Some(_) = p {...}
```

You can also add a "_" prefix to the variable that you don't use to get rid of rust warning:

```rust
    let s = Some(String::from("hello"));
    
    if let Some(_s) = s {
        println!("bound.");
    }
    //println!("{:?}", s); Note that even we don't use _s, s is still moved thus can't be referred anymore.
```

Note that even though ```_s``` is never used, the variable binding during pattern matching still happened, which means the ownership of s has been moved to ```_s```. In contrast, ```Some(_)``` won't trigger the binding thus s still holds the ownership.

#### Ignoring remaining part of a value with ```..```

With values that have many parts, we can use ```..``` to ignore them instead of using ```_``` repeatedly. 

```rust
match origin {
    Point { x, .. } => println!("x is {}", x),
}

let numbers = (2, 4, 8, 16, 32);
match numbers {
	(first, .., last) => println!("Some numbers: {}, {}", first, last),
    //(.., third, ..) => println!("This won't work.")
}
```

Note that ```..``` will expand to as many values as possible, so the commented arm in the above sample won't work.



### Extra Conditionals with Match Guards

A *match guard* is an additional `if` condition specified after the pattern in a `match` arm that must also match, along with the pattern matching, for that arm to be chosen. Match guards are useful for expressing more complex ideas than a pattern alone allows.

```rust
let num = Some(4);

match num {
    Some(x) if x < 5 => println!("less than five: {}", x),
    Some(x) => println!("{}", x),
    None => (),
}
```



### ```@``` Bindings

The at(```@```) operator let us create a variable so that we can test its value and also capture its value at the same time.

```rust

enum Message {
    Hello { id: i32 },
}

let msg = Message::Hello { id: 5 };

match msg {
    Message::Hello { id: id_variable @ 3..=7 } => {
        println!("Found an id in range: {}", id_variable)
    },
    Message::Hello { id: 10..=12 } => {
        println!("Found an id in another range")
    },
    Message::Hello { id } => {
        println!("Found some other id: {}", id)
    },
}
```



# Unsafe Rust

Rust's static code analysis is **conservative**, which means the compiler could reject valid code rather than taking risk to take code with potential bugs. Rust's ```unsafe``` keywork switches the compiler to unsafe mode, which basically tells the compiler"Trust me, I know what I'm doing".

Superpowers granted by unsafe rust:

* Dereference a raw pointer.
* Call unsafe function or method.
* Access or modify mutable static variable.
* Implement an unsafe trait
* Access fields of ```union```s.

Note that other safety features are still preserved in unsafe blocks, like borrow checking.

Keep unsafe blocks small!!!

## Dereferencing Raw Pointers

Raw pointers are unsafe rust features that are pretty much like C pointers. To define immutable and mutable raw pointers: ```*const T``` and ```*mut T```. In detail, they have the following differences than Rust's safe refernece:

* Are allowed to ignore borrowing rules by having both immutable and mutable pointers or multiple mutable pointers at the same time.
* Aren't guaranteed to point to valid memory.
* Are allowed to be null.
* Don't implement automatic cleanup.

Note that you don't need ```unsafe``` keyword to read raw pointers, you only need it when dereferencing it.

```rust
let mut num = 1;
let r1 = &num as *const i32;
let r2 = &num as *mut i32;  // Don't need unsafe so far.

unsafe {
    println!("r1 is {}", *r1);
    println!("r2 is {}", *r2);
}
```

## Calling Unsafe Functions or Methods

Unsafe function is defined exactly like regular rust function expect the prefixed ```unsafe``` keyword and must be called in a unsafe block:

```rust
unsafe fn dangerous() {
    //......
}

unsafe {
    dangerous();
}
```

Note that unsafe function bodies are essentially unsafe blocks, so there is no need to add unsafe blocks there, is redundant.

It's also very common to wrap unsafe functions/codes into a safe function so callers won't need to keep using unsafe blocks throughout the calling stack. But be aware that unsafe function might skip many rust's safe features, so it's the caller responsibility to manually rule out the potential risks. Here is an example in Rust's std library on how it safely split one mutable slice into two:

```rust
use std::slice;

fn split_at_mut(slice: &mut [i32], mid: usize) -> (&mut [i32], &mut [i32]) {
    let len = slice.len();
    let ptr = slice.as_mut_ptr(); //Convert a reference into a raw pointer.

    assert!(mid <= len); //Ensure that mid is a valid index.

    unsafe {
        (
            slice::from_raw_parts_mut(ptr, mid), //calculate raw pointer in C style. Note that here we must ensure the new raw pointer points to valid memory.
            slice::from_raw_parts_mut(ptr.add(mid), len - mid),
        )
    }
}
```

Note that function ```slice::from_raw_parts_mut``` is unsafe, so it's wrapped in a unsafe block thus making the whole function safe.

## Using ```extern``` Functions to Call External Code

The ```extern``` keyword facilities the creation and use of *Foreign Function Interface*(FFI) which is a way for programming language to define functions and enable a different programming language to call this function. Here is a example to call C's ```abs``` function:

 ```rust
extern "C" {
    fn abs(input: i32) -> i32;
}

fn main() {
    unsafe {
        println!("Abs of -3 is {}", abs(-3));
    }
}
 ```

Any external function is considered **unsafe** since they don't have any Rust's safety guarantee.

Within the `extern "C"` block, we list the names and signatures of external functions from another language we want to call. The `"C"` part defines which *application binary interface (ABI)* the external function uses: the ABI defines how to call the function at the assembly level. The `"C"` ABI is the most common and follows the C programming language’s ABI.



We can also use ```extern``` keyword to define functions that can be called by other languages:

```rust
#[no_mangle]
pub extern "C" fn call_from_c() {
    println!("Just called a Rust function from C!");
}
```

The annotation ```#[no_mangle]``` tells rust compiler not to change the functions name. Some compilers will change function name to contain more information but make it less readable by human.

## Accessing or Modifying a Mutable Static Variable

Rust also supports global variables(also called static variables in Rus), but it's not safe so generally not recommended to use.

```rust
static HELLO_WORLD: &str = "Hello, world!";

fn main() {
    println!("name is: {}", HELLO_WORLD);
}
```

Accessing a static immutable variable is safe, however, accessing mutable static variable needs to be in unsafe block:

```rust
static mut COUNTER: u32 = 0;

fn add_to_count(inc: u32) {
    unsafe {
        COUNTER += inc;
    }
}

fn main() {
    add_to_count(3);

    unsafe {
        println!("COUNTER: {}", COUNTER);
    }
}
```

## Implementing Unsafe Trait

Traits can be declared with ```unsafe``` keyword to annotate that it's not recommended to implement this trait. Implementing unsafe trait also requires unsafe keyword:

```rust
unsafe trait Foo {
    // methods go here
}

unsafe impl Foo for i32 {
    // method implementations go here
}
```

Common unsafe traits are ```Send``` and ```Sync```.



# Async Rust

## The ```Future``` Trait



futures-rs

tokio

hyper