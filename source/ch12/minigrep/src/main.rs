use std::env;
use std::process;
use minigrep::Config;

fn main() {
    // returns std::env::Args object which implements IntoIterator.
    let args: env::Args = env::args();

    let config = Config::new(args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    // Using if let, equivalent to unwrap_or_else
    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
