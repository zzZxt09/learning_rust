use futures::executor::block_on;
use std::{time, thread};

fn main() {
    let future = hello_world();

    // block_on function blocks the current calling thread until the provided future has run to completion
    // Blocking means the current thread will stop running and won't yield to let other task run here.
    block_on(future);

    block_on(
        async {

            thread::sleep(time::Duration::from_secs(1));
            let f_learn_n_song = learn_and_sing();
            let f_dance = dance();
            // `join!` is like `.await` but can wait for multiple futures concurrently.
            // If we're temporarily blocked in the `learn_and_sing` future, the `dance`
            // future will take over the current thread. If `dance` becomes blocked,
            // `learn_and_sing` can take back over. If both futures are blocked, then
            // `async_main` is blocked and will yield to the executor.
            futures::join!(f_dance, f_learn_n_song);
        }
    );
}

async fn hello_world() {
    println!("Hello, world.");
}

async fn learn_song() -> String {
    println!("learning a song......");
    thread::sleep(time::Duration::from_secs(10));
    String::from("Learned song.")
}

async fn sing_song(song: String) {
    println!("Sing the learned song {}.", song);
}

async fn dance() {
    println!("I'm dancing.");
}

// await won't block the thread, allowing it to carry other calculations.
// It's more like cascading CompletableFutures in java.
async fn learn_and_sing() {
    let song = learn_song().await;
    sing_song(song).await;
}
