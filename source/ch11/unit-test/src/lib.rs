#[derive(PartialEq, Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn new(width: u32, height: u32) -> Rectangle {
        Rectangle { width: width, height: height }
    }
}

fn panic() {
    panic!(String::from("Don't panic!"));
}

fn inspect_even(num :u32) -> Result<u32, String> {
    if num%2 == 0 {
        Result::Ok(num)
    } else {
        Result::Err(String::from("Not a even number"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Dumb test.
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }

    // Test with assert.
    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle { width: 8, height: 6 };
        let smaller = Rectangle { width: 6, height: 4 };

        assert!(larger.can_hold(&smaller));
        assert!(!smaller.can_hold(&larger),"Test failed.");
    }

    // Test with equal and not equal.
    #[test]
    fn test_rectangle_equals() {
        let rect1 = Rectangle::new(5,5);
        let rect2 = Rectangle::new(5,5);
        let rect3 = Rectangle::new(5,6);

        assert_eq!(rect1, rect2);
        assert_ne!(rect1, rect3);
    }

    // Test with expecting specific panic message.
    #[test]
    #[should_panic(expected = "Don't panic!")]
    fn test_panics() {
        panic();
    }

    // Test return Result<(), E>.
    #[test]
    fn test_even_with_result() -> Result<(), String> {
        inspect_even(4)?;
        Ok(())
    }

    #[test]
    #[ignore]
    fn test_ignore() {
        println!("Ignore me.");
    }



}
