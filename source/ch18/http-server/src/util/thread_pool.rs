use std::thread;
use std::thread::JoinHandle;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};


pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Job>
}

impl ThreadPool {
    /// Create a new ThreadPool,
    ///
    /// The size is the number of threads in the pool,
    ///
    /// # Panics
    ///
    /// The `new` function will panic if the size is zero.
    pub fn new(size: usize) -> ThreadPool{
        assert!(size > 0);

        let mut workers = Vec::with_capacity(size);
        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }
        ThreadPool{ workers, sender }
    }

    /// Submit a closure to the threadpool to execute.
    ///
    ///
    pub fn submit<F>(& self, f: F)
    where F: FnOnce() + Send + 'static {
        let job = Box::new(f);
        self.sender.send(job).unwrap();
    }
}

type Job = Box<dyn FnOnce() + Send + 'static>;

struct Worker {
    id: usize,
    thread: JoinHandle<()>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Job>>>)-> Worker {
        let thread = thread::spawn(move || loop {
            // This is important that returned MutexGuard object returend by lock() is a temporarily
            // acquired(not assigned to any variable), thus it will be dropped and unlocked after
            // this statement.
            let job = receiver.lock().unwrap().recv().unwrap();
            println!("Worker {} got a job, executing.", id);

            job();
        });

        Worker{ id, thread }
    }
}
