use std::net::TcpListener;
use std::net::TcpStream;
use std::io::Read;
use std::io::Write;

mod util;

pub use crate::util::thread_pool::ThreadPool;

fn main() {
    println!("Hello, world!");

    let thread_pool: ThreadPool = ThreadPool::new(10);
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();

    for stream in listener.incoming() {
        if let Err(_) = stream {
            println!("Failed to connect.");
        }
        let stream = stream.unwrap();
        thread_pool.submit(move || {
            handle_connection(stream);
        });

    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut buf = [0; 512];
    stream.read(&mut buf).unwrap();
    let response = "HTTP/1.1 200 OK\r\n\r\n";
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();

    println!("Received request: {}", String::from_utf8_lossy(&buf[..]));
}
