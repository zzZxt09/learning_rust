pub struct Post {
    state: Option<Box<dyn State>>,
    content: String,
}

impl Post {
    pub fn new() -> Post {
        Post {
            // Using Option here so we can take the ownership and leave a None here. Rust won't allow Null value anyway.
            state: Some(Box::new(Draft {})),
            content: String::new(),
        }
    }

    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }

    // Temporarily return empty string.
    pub fn content(&self) -> &str {
        if let Some(s) = self.state.as_ref() {
            if s.viewable() {
                return &self.content[..]
            } else {
                return ""
            }
        }
        ""
    }

    pub fn request_review(&mut self) {
        //The take() function will take the ownership of the optional and then s would be the new owner.
        //This function will consume the current state and return a new state.
        /*
        We need to set state to None temporarily rather than setting it directly with code like self.state = self.state.request_review(); to get ownership of the state value. This ensures Post can’t use the old state value after we’ve transformed it into a new state.
        */
        if let Some(s) = self.state.take() {
            self.state = Some(s.request_review());
        }
    }

    pub fn approve(&mut self) {
        if let Some(s) = self.state.take() {
            self.state = Some(s.approve());
        }
    }
}

trait State {
    // Using self: Box<Self> instead of &self to  take the ownership of the old state and  so it can transform to a new state.
    // Using syntax self: Box<Self> ensures only valid when called on a Box holding the same type.
    fn request_review(self: Box<Self>) -> Box<dyn State>;
    fn approve(self: Box<Self>) ->Box<dyn State>;
    fn viewable(&self) -> bool {
        false
    }
}

/**
    DRAFT STATE
*/
struct Draft {}

impl State for Draft {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        Box::new(PendingReview {})
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }
}

/**
    PENDING REVIEW STATE
*/
struct PendingReview {}

impl State for PendingReview {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        Box::new(Published {})
    }
}

/**
    PUBLISHED STATE
*/
struct Published {}

impl State for Published {
    fn request_review(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn approve(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn viewable(&self) -> bool {
        true
    }
}
