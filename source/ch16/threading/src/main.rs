use std::thread;
use std::time::Duration;
use std::sync::mpsc;
use std::sync::{Mutex, Arc};

fn main() {
    println!("Hello, world!");
    play_with_thread();
    play_with_channel();
    play_with_mutex();

}

fn play_with_thread() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("Hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(100));
        }
    });
    for i in 1..10 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(100));
    }

    handle.join().unwrap();

    let v = vec![1,2,3];

    // Need to use move keyword since we won't know the lifespan of v and the thread.
    let handle = thread::spawn( move || {
        println!("Here is the vector: {:?}", v);
    });

    handle.join().unwrap();
    //println!("Can't access v anymore from the main thread. {:?}", v);

}

fn play_with_channel() {
        // Using channel
    let (tx, rx) = mpsc::channel();
    // Copy the tx so we can send to the receiver from multiple sources.
    let tx1 = mpsc::Sender::clone(&tx);

    thread::spawn(move || {
        let vals = vec![
            String::from("Hello"),
            String::from("From"),
            String::from("The"),
            String::from("Spawned"),
            String::from("Thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
        //println!("Can't access val anymore {}", val);
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("Tx1"),
            String::from("Said"),
            String::from("Hi"),
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }

    });

    // rx only out of iteration when the channel is broken.
    for received in rx {
        println!("Received value: {}.", received);
    }

}


fn play_with_mutex() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("The final counting is {}", *counter.lock().unwrap());
}
