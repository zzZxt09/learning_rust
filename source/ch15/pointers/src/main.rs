use std::ops::Deref;


fn main() {
    println!("Hello, world!");

    let x = 5;
    let y = Box::new(5);
    println!("y={}", y);
    let z = &x;
    let k = &y;

    assert_eq!(5, x);

    assert_eq!(5, *z);

    assert_eq!(5, *y); // Use SmartPointer "Like" Reference
    assert_eq!(5, *y.deref()); // equivalent to above line
    //assert_eq!(5, y); // Fail: no implementation for `{integer} == std::boxed::Box<{integer}>`
    //assert_eq!(5, y.deref()); // Fail: no implementation for `{integer} == &{integer}`

    assert_eq!(5, **k);



    // Test for MyBox
    let m = MyBox::new(String::from("Hello, world!"));
    hello(&m);
    hello(m.deref());
    hello(&(*m)[..]);
    hello(&(*m.deref())[..]);
    hello(&(*m));

    let n = MyBox::new(String::from("Hello, Rust."));
    println!("{}", *n);
    println!("{}", *(n.deref()));
    assert_eq!("Hello, Rust.", *n);
}

struct MyBox<T>(T);

impl <T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

fn hello(m: &str) {
    println!("{}", m);
}
